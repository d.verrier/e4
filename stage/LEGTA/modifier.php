<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//vérifie que l'utilisateur connecté à pour statut "enseignant" ou "non-enseignant"

if ($_SESSION['statut']==1 || $_SESSION['statut']==6) {
?>
	<div class="container-fluid" align="center">
		<br>
		<br>
		<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #FFA500; border-right: 5px solid #FFA500">
			<br>
			<h1 style="font-family: 'Gentium Book Basic'">Formulaire de demande :</h1>
			<br>
		</div>
		<div class="tab-pane fade active show">
			<form class="form-horizontal" method="post" action="session.php">
				<div class="alert alert-secondary">
					<br>
					<p align="center"> Choisissez la demande à modifier :
						<select id="modification" name="modification">
<?php

						//requête de sélection permettant à l'utilisateur de choisir parmi ses demandes rangé par "dateSortie décroissante"  celle qui souhaite modifier

						$demande = $conn->query("SELECT num, destination, dateSortie, heureDepart, heureRetour FROM stage.sortie INNER JOIN stage.organise ON stage.sortie.num=stage.organise.sortie INNER JOIN stage.utilisateur ON stage.sortie.connecte=stage.utilisateur.id WHERE organise.centre = 1 AND sortie.connecte='".$_SESSION['id']."' ORDER BY dateSortie desc;");
						while($liste_demande = $demande->fetch()){
?>
							<option value=<?php echo "".$liste_demande['num']?>> <?php echo $liste_demande['destination']," le ",$liste_demande['dateSortie']," de ",$liste_demande['heureDepart']," à ",$liste_demande['heureRetour'] ;?></option>
<?php
						}
?>
						</select>
						<button type="submit" class="btn btn-info">Valider</button>
					</p>
				</div>
			</form>
			<hr>
			<br>
<?php

			//vérifie que l'utilisateur est choisi sa sortie à modifier

			if(isset($_SESSION['demande_modification']) && $_SESSION['demande_modification'] == 1) {
				if(isset($_SESSION['demande_Vem']) && $_SESSION['demande_Vem'] == 1) {
?>
					<form class="form-horizontal" method="post" action = "modification.php">
						<div class="alert alert-secondary">
							<br>
							<h3 style="color: red;" align="center">Attention : </h3>
<?php
							if(isset($_SESSION['verification1']) && $_SESSION['verification1'] == 1) {
?>
								<br>
								<h5 style="color: red;">La date de sortie est inférieure ou égale à la date d'aujourd'hui ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification2']) && $_SESSION['verification2'] == 1) {
?>
								<br>
								<h5 style="color: red;">L'heure de retour doit être supérieur à l'heure de départ ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification3']) && $_SESSION['verification3'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez cocher les classes participant à la sortie ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification4']) && $_SESSION['verification4'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez cocher les accompagnateurs participant à la sortie ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification5']) && $_SESSION['verification5'] == 1) {
?>
								<br>
								<h5 style="color: red;">Nombre total d'apprenants = Interne(s) + Demi-Pensionnaire(s) + Externe(s) ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification6']) && $_SESSION['verification6'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez indiquer une heure d'enlèvement en cuisine ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification7']) && $_SESSION['verification7'] == 1) {
?>
								<br>
								<h5 style="color: red;">Le nombre de repas végétariens est supérieur au nombre total de repas ! </h5>
<?php
							}
?>
							<br>
							<hr style="border-color: green">
							<p align="center"><b>Résidence administrative :</b> EPLEFPA La Germinière 72700 ROUILLON&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							<b>Centre : </b>LEGTA</p>
							<hr style="border-color: green">
							<label for="nature"><b>Nature de la mission :</b></label>
							<input class="form-control" id="nature" name="nature" placeholder="Objectif..." type="text" value="<?php echo $_SESSION['natureVe'] ?>"/>
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="dateSortie"><b>Date de sortie :</b></label>
									<input class="form-control" id="dateSortie" name="dateSortie" type="date" value="<?php echo $_SESSION['dateSortieVe'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureDepart"><b>Heure de départ : </b></label>
									<input class="form-control" id="heureDepart" name="heureDepart" type="time" value="<?php echo $_SESSION['heureDepartVe'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureRetour"><b>Heure de retour : </b></label>
									<input class="form-control" id="heureRetour" name="heureRetour" type="time" value="<?php echo $_SESSION['heureRetourVe'] ?>" required />
								</div>
							</div>
							<label for="destination"><b>Destination :</b></label>
							<input class="form-control" id="destination" name="destination" type="text" value="<?php echo $_SESSION['destinationVe'] ?>" required />
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="NbKm"><b>Nombre de kilomètres A/R : </b></label>
									<input class="form-control" id="NbKm" name="NbKm" min="0" type="number" value="<?php echo $_SESSION['NbKmVe'] ?>" required />
								</div>
							</div>
							<hr style="border-color: green">
							<label for="lienReferentiel"><b>Lien avec le référentiel :</b></label>
							<input class="form-control" id="lienReferentiel" name="lienReferentiel" type="text" value="<?php echo $_SESSION['lienReferentielVe'] ?>" />
							<br>
							<b><a target="_blank" href="http://s509545027.onlinehome.fr/grrminiere/login.php">Véhicule(s) emprunté(s) (consulter GRR pour les disponibilités)</a></b>
							<br>
							<br>
	<?php
							$tabT5 = array();
							foreach ($_SESSION['vehiculesVe'] as $utiliseVVe) {
								$tabT5[$utiliseVVe]="checked";
							}

							//requête de sélection de l'immatriculation et du modèle de la table "véhicule"

							$utilise = $conn->query("SELECT num, immatriculation, modele from stage.vehicule;");
							$i=0;
							while ($liste_utilise = $utilise->fetch()){
								if (isset($tabT5[$liste_utilise['num']])) {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="vehicules[]" id="<?php echo 'vehicules' . $i; ?>" value="<?php echo $liste_utilise['num']; ?>" <?php echo "checked"; ?>>
								<label for="<?php echo 'vehicules' . $i; ?>"><i>
	<?php
										echo $liste_utilise['immatriculation'];
										echo ' - <b>' . $liste_utilise['modele'];
	?>
										</b></i></label>
	<?php
								}
								else {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="vehicules[]" id="<?php echo 'vehicules' . $i; ?>" value="<?php echo $liste_utilise['num']; ?>">
								<label for="<?php echo 'vehicules' . $i; ?>"><i>
	<?php
										echo $liste_utilise['immatriculation'];
										echo ' - <b>' . $liste_utilise['modele'];
	?>
										</b></i></label>
	<?php
								}
								$i = $i + 1;
								if ($i % 4 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							&nbsp; &nbsp; &nbsp;
							<br>
							<label for="nbTicketSetram"><b>Bus SETRAM :</b></label><br><i><label for="nbTicketSetram"><b>Nombre de tickets à prévoir :</b></label></i><input class="form-control" id="nbTicketSetram" name="nbTicketSetram" min="0" type="number" value="<?php echo $_SESSION['nbTicketSetramVe'] ?>" />
							<br>
							<hr style="border-color: green">
							<b>Classe(s) ou groupe(s) :</b>
							<br>
							<br>
	<?php
							$tabT6 = array();
							foreach ($_SESSION['classesVe'] as $concerneCVe) {
								$tabT6[$concerneCVe]="checked";
							}

							//requête de sélection du num et du libelle de la table "classe"

							$concerne = $conn->query("SELECT num, libelle from stage.classe;");
							$i=0;
							while ($liste_concerne = $concerne->fetch()){
								if (isset($tabT6[$liste_concerne['num']])) {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="classes[]" id="<?php echo 'classes' . $i; ?>" value="<?php echo $liste_concerne['num']; ?>" <?php echo "checked"; ?>>
								<label for="<?php echo 'classes' . $i; ?>"><i>
	<?php
										echo $liste_concerne['libelle'];
	?>
										</b></i></label>
	<?php
								}
								else {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="classes[]" id="<?php echo 'classes' . $i; ?>" value="<?php echo $liste_concerne['num']; ?>">
								<label for="<?php echo 'classes' . $i; ?>"><i>
	<?php
										echo $liste_concerne['libelle'];
	?>
										</b></i></label>
	<?php
								}
								$i = $i + 1;
								if ($i % 5 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="10" style="background-color: green; color: white" value="<?php echo "Autre centre" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre d'apprenants" ?> "><br><br>
								<select name="centrei1" id="centrei1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT2 = array();
							$tabT2[$_SESSION['centrei1Ve']]="selected";

							//requête de sélection du num et du libelle de la table "centre"

							$centrei1 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei1 = $centrei1->fetch()){
								if (isset($tabT2[$liste_centrei1['num']])) {
	?>
									<option value=<?php echo "".$liste_centrei1['num']?> <?php echo "selected"; ?>> <?php echo $liste_centrei1['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_centrei1['num']?>> <?php echo $liste_centrei1['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei1" id="Classei1" value="<?php echo $_SESSION['Classei1Ve'] ?>"> &nbsp;&nbsp; <input type="number" min="0" name="nbi1" id="nbi1" value="<?php echo $_SESSION['nbi1Ve'] ?>"><br><br>
								<select name="centrei2" id="centrei2" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT3 = array();
							$tabT3[$_SESSION['centrei2Ve']]="selected";

							//requête de sélection du num et du libelle de la table "centre"

							$centrei2 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei2 = $centrei2->fetch()){
								if (isset($tabT3[$liste_centrei2['num']])) {
	?>
									<option value=<?php echo "".$liste_centrei2['num']?> <?php echo "selected"; ?>> <?php echo $liste_centrei2['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_centrei2['num']?>> <?php echo $liste_centrei2['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei2" id="Classei2" value="<?php echo $_SESSION['Classei2Ve'] ?>"> &nbsp;&nbsp; <input type="number" min="0" name="nbi2" id="nbi2" value="<?php echo $_SESSION['nbi2Ve'] ?>"><br><br>
							</div>
							<br>
							<hr>
							<label for="nbEleves"><b>Nombre total d'apprenants :</b></label>
							<input class="form-control" id="nbEleves" name="nbEleves" type="number" min="0" value="<?php echo $_SESSION['nbElevesVe'] ?>" required />
							<br>
							<p align="center"> <b>Responsable :</b>
							<br>
							<br>
							<select id="responsable" name="responsable" required>
	<?php
							$tabT1 = array();
							$tabT1[$_SESSION['responsableVe']]="selected";

							//requête de sélection de l'id, du prénom et du nom de la table "utilisateur"

							$responsable = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
							while($liste_responsable = $responsable->fetch()){
								if (isset($tabT1[$liste_responsable['id']])) {
	?>
									<option value=<?php echo "".$liste_responsable['id']?> <?php echo "selected"; ?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_responsable['id']?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
							}
	?>
							</select>
							<hr style="border-color: green">
							<b>Accompagnateur(s) :</b>
							<br>
							<br>
							<select multiple name="utilisateurs[]" id="utilisateurs[]" size="5" >
	<?php
								$tabT4 = array();
								foreach ($_SESSION['utilisateursVe'] as $accompagneUVe) {
									$tabT4[$accompagneUVe]="selected";
								}

								//requête de sélection de l'id, du prénom et du nom de la table "utilisateur" rangé par nom "croissant"

								$accompagne = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
								while ($liste_accompagne = $accompagne->fetch()){
									if (isset($tabT4[$liste_accompagne['id']])) {
	?>
										<option value=<?php echo "".$liste_accompagne['id']?> <?php echo "selected"; ?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
									else {
	?>
										<option value=<?php echo "".$liste_accompagne['id']?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
								}
	?>
							</select>
							<br>
							(Ctrl+Clic)
							<hr style="border-color: green">
							<p align="center"> <b>Remplacement(s) :</b>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Cours concerné" ?> "> &nbsp;&nbsp; <input disabled type="text" style="background-color: green; color: white" value="<?php echo "Remplacement proposé" ?> "><br><br>
								<select name="classe1" id="classe1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT7 = array();
							$tabT7[$_SESSION['classe1Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer1 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe1 = $classer1->fetch()){
								if (isset($tabT7[$liste_classe1['num']])) {
	?>
									<option value=<?php echo "".$liste_classe1['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe1['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe1['num']?>> <?php echo $liste_classe1['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours1" id="Cours1" value="<?php echo $_SESSION['Cours1Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition1" id="proposition1" value="<?php echo $_SESSION['proposition1Ve'] ?>"><br><br>
								<select name="classe2" id="classe2">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT8 = array();
							$tabT8[$_SESSION['classe2Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer2 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe2 = $classer2->fetch()){
								if (isset($tabT8[$liste_classe2['num']])) {
	?>
									<option value=<?php echo "".$liste_classe2['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe2['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe2['num']?>> <?php echo $liste_classe2['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours2" id="Cours2" value="<?php echo $_SESSION['Cours2Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition2" id="proposition2" value="<?php echo $_SESSION['proposition2Ve'] ?>"><br><br>
								<select name="classe3" id="classe3">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT9 = array();
							$tabT9[$_SESSION['classe3Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer3 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe3 = $classer3->fetch()){
								if (isset($tabT9[$liste_classe3['num']])) {
	?>
									<option value=<?php echo "".$liste_classe3['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe3['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe3['num']?>> <?php echo $liste_classe3['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours3" id="Cours3" value="<?php echo $_SESSION['Cours3Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition3" id="proposition3" value="<?php echo $_SESSION['proposition3Ve'] ?>"><br><br>
								<select name="classe4" id="classe4">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT10 = array();
							$tabT10[$_SESSION['classe4Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer4 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe4 = $classer4->fetch()){
								if (isset($tabT10[$liste_classe4['num']])) {
	?>
									<option value=<?php echo "".$liste_classe4['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe4['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe4['num']?>> <?php echo $liste_classe4['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours4" id="Cours4" value="<?php echo $_SESSION['Cours4Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition4" id="proposition4" value="<?php echo $_SESSION['proposition4Ve'] ?>"><br><br>
								<select name="classe5" id="classe5">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT11 = array();
							$tabT11[$_SESSION['classe5Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer5 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe5 = $classer5->fetch()){
								if (isset($tabT11[$liste_classe5['num']])) {
	?>
									<option value=<?php echo "".$liste_classe5['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe5['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe5['num']?>> <?php echo $liste_classe5['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours5" id="Cours5" value="<?php echo $_SESSION['Cours5Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition5" id="proposition5" value="<?php echo $_SESSION['proposition5Ve'] ?>"><br><br>
								<select name="classe6" id="classe6">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT12 = array();
							$tabT12[$_SESSION['classe6Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer6 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe6 = $classer6->fetch()){
								if (isset($tabT12[$liste_classe6['num']])) {
	?>
									<option value=<?php echo "".$liste_classe6['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe6['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe6['num']?>> <?php echo $liste_classe6['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours6" id="Cours6" value="<?php echo $_SESSION['Cours6Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition6" id="proposition6" value="<?php echo $_SESSION['proposition6Ve'] ?>"><br><br>
								<select name="classe7" id="classe7">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT13 = array();
							$tabT13[$_SESSION['classe7Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer7 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe7 = $classer7->fetch()){
								if (isset($tabT13[$liste_classe7['num']])) {
	?>
									<option value=<?php echo "".$liste_classe7['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe7['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe7['num']?>> <?php echo $liste_classe7['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours7" id="Cours7" value="<?php echo $_SESSION['Cours7Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition7" id="proposition7" value="<?php echo $_SESSION['proposition7Ve'] ?>"><br><br>
								<select name="classe8" id="classe8">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT14 = array();
							$tabT14[$_SESSION['classe8Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer8 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe8 = $classer8->fetch()){
								if (isset($tabT14[$liste_classe8['num']])) {
	?>
									<option value=<?php echo "".$liste_classe8['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe8['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe8['num']?>> <?php echo $liste_classe8['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours8" id="Cours8" value="<?php echo $_SESSION['Cours8Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition8" id="proposition8" value="<?php echo $_SESSION['proposition8Ve'] ?>"><br><br>
							</div>
							<hr style="border-color: green">
							<b>Choix du repas :</b>
							<br>
							&nbsp; &nbsp; &nbsp;
							<div class="row">
								&nbsp; &nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasNon" value="Pas de Repas" <?php if( $_SESSION['RepasVe'] == "Pas de Repas"){  echo "checked"; }?> >
								<label for="RepasNon"><i>Pas de repas</i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasSecs" value="Repas Secs" <?php if( $_SESSION['RepasVe'] == "Repas Secs"){  echo "checked"; }?> >
								<label for="RepasSecs"><i>Repas Secs (</i>à utiliser pour déplacements, <i><b>pas de conservation au froid)</b></i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasRefrigeres" value="Repas Réfrigérés" <?php if( $_SESSION['RepasVe'] == "Repas Réfrigérés"){  echo "checked"; }?> >
								<label for="RepasRefrigeres"><i>Repas réfrigérés (</i>repas complet <i><b>tenus au frais en glacière)</b></i></label>
							</div>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Repas Végétariens" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre de repas " ?> "><br><br>
								<select name="vegetarien" id="vegetarien" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							if ($_SESSION['vegetarienVe'] == "Oui") {
	?>
								<option value="Oui" selected >Oui</option>
								<option value="Non">Non</option>
	<?php
							}
							else if ($_SESSION['vegetarienVe'] == "Non") {
	?>
								<option value="Oui">Oui</option>
								<option value="Non" selected >Non</option>
	<?php
							}
							else {
	?>
								<option value="Oui">Oui</option>
								<option value="Non">Non</option>
	<?php
							}
	?>
								</select>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" min="0" name="nbVegetarien" id="nbVegetarien" value="<?php echo $_SESSION['nbVegetarienVe'] ?>"><br><br>
							</div>
							<br>
							<br>
							<b>Nombre total de repas à réserver : </b>
							<br>
							<hr>
							<br>
							<b>Apprenants :</b>
							<br>
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="nbInterne"><b>Interne(s) : </b></label>
									<input class="form-control" id="nbInterne" name="nbInterne" type="number" min="0" value="<?php echo $_SESSION['nbInterneVe'] ?>" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbDP"><b>Demi-Pensionnaire(s) : </b></label>
									<input class="form-control" id="nbDP" name="nbDP" type="number" min="0" value="<?php echo $_SESSION['nbDPVe'] ?>" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbExterne"><b>Externe(s) : </b></label>
									<input class="form-control" id="nbExterne" name="nbExterne" type="number" min="0" value="<?php echo $_SESSION['nbExterneVe'] ?>" />
								</div>
							</div>
							<hr>
							<label for="heureEnlevement"><b>Heure d'enlèvement en cuisine : </b>
							<input class="form-control" id="heureEnlevement" name="heureEnlevement" type="time" value="<?php echo $_SESSION['heureEnlevementVe'] ?>" /></label>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
<?php
				}
				else {
?>
				<form class="form-horizontal" method="post" action = "modification.php">
<?php

				//requête permettant de récupérer toutes les valeurs de la sortie sélectionnée

				$requete_modification = $conn->query("SELECT * FROM stage.sortie INNER JOIN stage.organise ON stage.sortie.num=stage.organise.sortie WHERE organise.centre=1 AND sortie.num=".$_SESSION['modification'].";");
				$donnees = $requete_modification->fetch();
?>
					<div class="alert alert-secondary">
						<p align="center"><b>Résidence administrative :</b> EPLEFPA La Germinière 72700 ROUILLON&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<b>Centre : </b>LEGTA</p>
						<hr style="border-color: green">
						<label for="nature"><b>Nature de la mission :</b></label>
						<input class="form-control" id="nature" name="nature" type="text" placeholder="Objectif..." value="<?php echo $donnees['nature'] ?>" />
						<br>
						<div class="row">
							<div class="form-group col text-center">
								<label for="dateSortie"><b>Date de sortie :</b></label>
								<input class="form-control" id="dateSortie" name="dateSortie" type="date" value="<?php echo $donnees['dateSortie'] ?>" required />
							</div>
							&nbsp; &nbsp; &nbsp; &nbsp;
							<div class="form-group col text-center">
								<label for="heureDepart"><b>Heure de départ : </b></label>
								<input class="form-control" id="heureDepart" name="heureDepart" type="time" value="<?php echo $donnees['heureDepart'] ?>" required />
							</div>
							&nbsp; &nbsp; &nbsp; &nbsp;
							<div class="form-group col text-center">
								<label for="heureRetour"><b>Heure de retour : </b></label>
								<input class="form-control" id="heureRetour" name="heureRetour" type="time" value="<?php echo $donnees['heureRetour'] ?>" required />
							</div>
						</div>
						<label for="destination"><b>Destination :</b></label>
						<input class="form-control" id="destination" name="destination" type="text" value="<?php echo $donnees['destination'] ?>" required/>
						<hr style="border-color: green">
						<div class="row">
							<div class="form-group col text-center">
								<label for="NbKm"><b>Nombre de kilomètres A/R : </b></label>
								<input class="form-control" id="NbKm" name="NbKm" min="0" type="number" value="<?php echo $donnees['nbKm'] ?>" />
							</div>
						</div>
						<hr style="border-color: green">
						<label for="lienReferentiel"><b>Lien avec le référentiel :</b></label>
						<input class="form-control" id="lienReferentiel" name="lienReferentiel" type="text" value="<?php echo $donnees['lienReferentiel'] ?>" />
						<br>
						<b><a target="_blank" href="http://s509545027.onlinehome.fr/grrminiere/login.php">Véhicule(s) emprunté(s) (consulter GRR pour les disponibilités)</a></b>
						<br>
						<br>
<?php

						//requête permettant de récupérer l'immatriculation de la table "utilise" en fonction du "num" de la sortie sélectionnée

						$srequete1 = "SELECT vehicule FROM stage.utilise where sortie = ".$_SESSION['modification'].";";
						$liste_utilise = $conn->query($srequete1);
						$tabV = array();

						while ($immatriculation_utilise = $liste_utilise->fetch()){
							$tabV[$immatriculation_utilise['vehicule']]="checked";
						}

						//requête permettant de récupérer toutes les immatriculations et modèles de la table "vehicule"

						$requete1 = "SELECT num, immatriculation, modele from stage.vehicule;";
						$utilise = $conn->query($requete1);
						$i = 0;

						while ($immatriculation = $utilise->fetch()){
?>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<?php
							if (isset($tabV[$immatriculation['num']])) {
?>
								<input type="checkbox" name="vehicules[]" id=" <?php echo $immatriculation['num']; ?>" value="<?php echo $immatriculation['num']; ?>" <?php echo "checked"; ?> >
<?php
								echo $immatriculation['immatriculation'];
								echo ' - <b>' . $immatriculation['modele'];
?>
								</b>
<?php
							}
							else {
?>
								<input type="checkbox" name="vehicules[]" id=" <?php echo $immatriculation['num']; ?>" value="<?php echo $immatriculation['num']; ?>" >
<?php
								echo $immatriculation['immatriculation'];
								echo ' - <b>' . $immatriculation['modele'];
?>
								</b>
<?php
							}
							$i = $i + 1;
							if ($i % 4 == 0){
?>
								<br>
<?php
							}
						}
?>
						<br>
						&nbsp; &nbsp; &nbsp;
						<br>
						<label for="nbTicketSetram"><b>Bus SETRAM :</b></label><br><i><label for="nbTicketSetram"><b>Nombre de tickets à prévoir :</b></label></i><input class="form-control" id="nbTicketSetram" name="nbTicketSetram" min="0" type="number" value="<?php echo $donnees['nbTicketSetram'] ?>" />
						<br>
						<hr style="border-color: green">
						<b>Classe(s) ou groupe(s) :</b>
						<br>
						<br>
<?php

						//requête permettant de récupérer la classe de la table "concerne" en fonction du "num" de la sortie sélectionnée

						$srequete2 = "SELECT classe from stage.concerne where sortie = ".$_SESSION['modification'].";";
						$liste_concerne = $conn->query($srequete2);
						$tabC = array();

						while ($num_concerne = $liste_concerne->fetch()){
							$tabC[$num_concerne['classe']]="checked";
						}

						//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

						$requete2 = "SELECT num, libelle from stage.classe;";
						$concerne = $conn->query($requete2);
						$i=0;

						while ($numero_C = $concerne->fetch()){
?>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<?php
							if (isset($tabC[$numero_C['num']])) {
?>
								<input type="checkbox" name="classes[]" id=" <?php echo $numero_C['num']; ?>" value="<?php echo $numero_C['num']; ?>" <?php echo "checked"; ?> >
<?php
								echo $numero_C['libelle'];
							}
							else {
?>
								<input type="checkbox" name="classes[]" id=" <?php echo $numero_C['num']; ?>" value="<?php echo $numero_C['num']; ?>" >
<?php
								echo $numero_C['libelle'];
							}
							$i = $i + 1;
							if ($i % 5 == 0){
?>
								<br>
<?php
							}
						}
?>
						<br>
						<hr>
						<div class="table-responsive bg-light">
							<br>
							<input disabled type="text" size="10" style="background-color: green; color: white" value="<?php echo "Autre centre" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre d'apprenants" ?> "><br><br>
							<select name="centrei1" id="centrei1" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer le centre de la table "invite" en fonction du "num" de la sortie sélectionnée

								$sirequetesTest1 = "SELECT centre from stage.invite WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 0,1;";
								$siliste_Test1 = $conn->query($sirequetesTest1);
								$sitabT1 = array();

								while ($siid_test1 = $siliste_Test1->fetch()){
									$sitabT1[$siid_test1['centre']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$siclasses1 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
								while($siliste_classes1 = $siclasses1->fetch()){
									if (isset($sitabT1[$siliste_classes1['num']])) {
?>
										<option value=<?php echo "".$siliste_classes1['num']?> <?php echo "selected"; ?>> <?php echo $siliste_classes1['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$siliste_classes1['num']?>> <?php echo $siliste_classes1['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer la classe et le nombre de la table "invite" en fonction du "num" de la sortie sélectionnée

							$sirequete_remplacement1 = $conn->query("SELECT classe, nombre FROM stage.invite WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 0,1;");
							$sidonnees1 = $sirequete_remplacement1->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Classei1" id="Classei1" value="<?php echo $sidonnees1['classe'] ?>" >
							&nbsp;&nbsp; <input type="number" min="0" name="nbi1" id="nbi1" value="<?php echo $sidonnees1['nombre'] ?>" >
							<br>
							<br>
							<select name="centrei2" id="centrei2" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer le centre de la table "invite" en fonction du "num" de la sortie sélectionnée

								$sirequetesTest2 = "SELECT centre from stage.invite WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 1,1;";
								$siliste_Test2 = $conn->query($sirequetesTest2);
								$sitabT2 = array();

								while ($siid_test2 = $siliste_Test2->fetch()){
									$sitabT2[$siid_test2['centre']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$siclasses2 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
								while($siliste_classes2 = $siclasses2->fetch()){
									if (isset($sitabT2[$siliste_classes2['num']])) {
?>
										<option value=<?php echo "".$siliste_classes2['num']?> <?php echo "selected"; ?>> <?php echo $siliste_classes2['libelle'] ;?></option>
<?php
									}
									else {
?>	
										<option value=<?php echo "".$siliste_classes2['num']?>> <?php echo $siliste_classes2['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer la classe et le nombre de la table "invite" en fonction du "num" de la sortie sélectionnée

							$sirequete_remplacement2 = $conn->query("SELECT classe, nombre FROM stage.invite WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 1,1;");
							$sidonnees2 = $sirequete_remplacement2->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Classei2" id="Classei2" value="<?php echo $sidonnees2['classe'] ?>" >
							&nbsp;&nbsp; <input type="number" min="0" name="nbi2" id="nbi2" value="<?php echo $sidonnees2['nombre'] ?>" >
							<br>
							<br>
						</div>
						<br>
						<hr>
						<label for="nbEleves"><b>Nombre total d'apprenants :</b></label>
						<input class="form-control" id="nbEleves" name="nbEleves" type="number" min="0" value="<?php echo $donnees['nbEleves'] ?>" required />
						<br>
						<p align="center"> <b>Responsable :</b>
						<br>
						<br>
						<select id="responsable" name="responsable" >
<?php

						//requête permettant de récupérer le responsable de la table "sortie" en fonction du "num" de la sortie sélectionnée

						$srequeteTest1 = "SELECT responsable from stage.sortie where num = ".$_SESSION['modification'].";";
						$liste_Test1 = $conn->query($srequeteTest1);
						$tabT1 = array();

						while ($id_test1 = $liste_Test1->fetch()){
							$tabT1[$id_test1['responsable']]="selected";
						}

						//requête permettant de récupérer tous les "id", "prenom" et "nom" de la table "utilisateur" rangé par nom "croissant"

						$responsable = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
						while($liste_responsable = $responsable->fetch()){
							if (isset($tabT1[$liste_responsable['id']])) {
?>
								<option value=<?php echo "".$liste_responsable['id']?> <?php echo "selected"; ?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
<?php
							}
							else {
?>
								<option value=<?php echo "".$liste_responsable['id']?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
<?php
							}
						}
?>
						</select>
						<hr style="border-color: green">
						<b>Accompagnateur(s) :</b>
						<br>
						<br>
						<select multiple name="utilisateurs[]" id="utilisateurs[]" size="5" >
<?php

						//requête permettant de récupérer l'utilisateur de la table "sortie" en fonction du "num" de la sortie sélectionnée

						$srequete3 = "SELECT utilisateur from stage.accompagne where sortie = ".$_SESSION['modification'].";";
						$liste_accompagne = $conn->query($srequete3);
						$tabA = array();

						while ($utilisateur_accompagne = $liste_accompagne->fetch()){
							$tabA[$utilisateur_accompagne['utilisateur']]="selected";
						}

						//requête permettant de récupérer tous les "id", "prenom" et "nom" de la table "utilisateur"

						$requete3 = "SELECT id, prenom, nom from stage.utilisateur order by nom asc;";
						$accompagne = $conn->query($requete3);

						while ($idA = $accompagne->fetch()){
?>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<?php
							if (isset($tabA[$idA['id']])) {
?>
								<option value=<?php echo "".$idA['id']?> <?php echo "selected"; ?>> <?php echo $idA['prenom']," ",$idA['nom'] ;?></option>
<?php
							}
							else {
?>
								<option value=<?php echo "".$idA['id']?>> <?php echo $idA['prenom']," ",$idA['nom'] ;?></option>
<?php
							}
						}
?>
						</select>
						<br>
						(Ctrl+Clic)
						<hr style="border-color: green">
						<p align="center"> <b>Remplacement(s) :</b>
						<div class="table-responsive bg-light">
							<br>
							<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Cours concerné" ?> "> &nbsp;&nbsp; <input disabled type="text" style="background-color: green; color: white" value="<?php echo "Remplacement proposé" ?> "><br><br>						
							<select id="classe1" name="classe1" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest1 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 0,1;";
								$sliste_Test1 = $conn->query($srequetesTest1);
								$stabT1 = array();

								while ($sid_test1 = $sliste_Test1->fetch()){
									$stabT1[$sid_test1['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses1 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes1 = $sclasses1->fetch()){
									if (isset($stabT1[$sliste_classes1['num']])) {
?>
										<option value=<?php echo "".$sliste_classes1['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes1['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes1['num']?>> <?php echo $sliste_classes1['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement1 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 0,1;");
							$sdonnees1 = $srequete_remplacement1->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours1" id="Cours1" value="<?php echo $sdonnees1['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition1" id="proposition1" value="<?php echo $sdonnees1['proposition'] ?>" >
							<br>
							<br>
							<select id="classe2" name="classe2" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest2 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 1,1;";
								$sliste_Test2 = $conn->query($srequetesTest2);
								$stabT2 = array();

								while ($sid_test2 = $sliste_Test2->fetch()){
									$stabT2[$sid_test2['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses2 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes2 = $sclasses2->fetch()){
									if (isset($stabT2[$sliste_classes2['num']])) {
?>
										<option value=<?php echo "".$sliste_classes2['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes2['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes2['num']?>> <?php echo $sliste_classes2['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement2 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 1,1;");
							$sdonnees2 = $srequete_remplacement2->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours2" id="Cours2" value="<?php echo $sdonnees2['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition2" id="proposition2" value="<?php echo $sdonnees2['proposition'] ?>" >
							<br>
							<br>
							<select id="classe3" name="classe3" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest3 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 2,1;";
								$sliste_Test3 = $conn->query($srequetesTest3);
								$stabT3 = array();

								while ($sid_test3 = $sliste_Test3->fetch()){
									$stabT3[$sid_test3['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses3 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes3 = $sclasses3->fetch()){
									if (isset($stabT3[$sliste_classes3['num']])) {
?>
										<option value=<?php echo "".$sliste_classes3['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes3['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes3['num']?>> <?php echo $sliste_classes3['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement3 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 2,1;");
							$sdonnees3 = $srequete_remplacement3->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours3" id="Cours3" value="<?php echo $sdonnees3['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition3" id="proposition3" value="<?php echo $sdonnees3['proposition'] ?>" >
							<br>
							<br>
							<select id="classe4" name="classe4" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest4 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 3,1;";
								$sliste_Test4 = $conn->query($srequetesTest4);
								$stabT4 = array();

								while ($sid_test4 = $sliste_Test4->fetch()){
									$stabT4[$sid_test4['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses4 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes4 = $sclasses4->fetch()){
									if (isset($stabT4[$sliste_classes4['num']])) {
?>
										<option value=<?php echo "".$sliste_classes4['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes4['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes4['num']?>> <?php echo $sliste_classes4['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement4 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 3,1;");
							$sdonnees4 = $srequete_remplacement4->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours4" id="Cours4" value="<?php echo $sdonnees4['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition4" id="proposition4" value="<?php echo $sdonnees4['proposition'] ?>" >
							<br>
							<br>
							<select id="classe5" name="classe5" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest5 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 4,1;";
								$sliste_Test5 = $conn->query($srequetesTest5);
								$stabT5 = array();

								while ($sid_test5 = $sliste_Test5->fetch()){
									$stabT5[$sid_test5['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses5 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes5 = $sclasses5->fetch()){
									if (isset($stabT5[$sliste_classes5['num']])) {
?>
										<option value=<?php echo "".$sliste_classes5['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes5['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes5['num']?>> <?php echo $sliste_classes5['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement5 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 4,1;");
							$sdonnees5 = $srequete_remplacement5->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours5" id="Cours5" value="<?php echo $sdonnees5['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition5" id="proposition5" value="<?php echo $sdonnees5['proposition'] ?>" >
							<br>
							<br>
							<select id="classe6" name="classe6" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest6 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 5,1;";
								$sliste_Test6 = $conn->query($srequetesTest6);
								$stabT6 = array();

								while ($sid_test6 = $sliste_Test6->fetch()){
									$stabT6[$sid_test6['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses6 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes6 = $sclasses6->fetch()){
									if (isset($stabT6[$sliste_classes6['num']])) {
?>
										<option value=<?php echo "".$sliste_classes6['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes6['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes6['num']?>> <?php echo $sliste_classes6['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement6 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 5,1;");
							$sdonnees6 = $srequete_remplacement6->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours6" id="Cours6" value="<?php echo $sdonnees6['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition6" id="proposition6" value="<?php echo $sdonnees6['proposition'] ?>" >
							<br>
							<br>
							<select id="classe7" name="classe7" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest7 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 6,1;";
								$sliste_Test7 = $conn->query($srequetesTest7);
								$stabT7 = array();

								while ($sid_test7 = $sliste_Test7->fetch()){
									$stabT7[$sid_test7['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses7 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes7 = $sclasses7->fetch()){
									if (isset($stabT7[$sliste_classes7['num']])) {
?>
										<option value=<?php echo "".$sliste_classes7['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes7['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes7['num']?>> <?php echo $sliste_classes7['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement7 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 6,1;");
							$sdonnees7 = $srequete_remplacement7->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours7" id="Cours7" value="<?php echo $sdonnees7['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition7" id="proposition7" value="<?php echo $sdonnees7['proposition'] ?>" >
							<br>
							<br>
							<select id="classe8" name="classe8" >
								<option selected disabled hidden>Choisissez ici</option>
<?php

								//requête permettant de récupérer la classe de la table "remplacement" en fonction du "num" de la sortie sélectionnée

								$srequetesTest8 = "SELECT classe from stage.remplacement WHERE sortie = ".$_SESSION['modification']." ORDER BY num asc limit 7,1;";
								$sliste_Test8 = $conn->query($srequetesTest8);
								$stabT8 = array();

								while ($sid_test8 = $sliste_Test8->fetch()){
									$stabT8[$sid_test8['classe']]="selected";
								}

								//requête permettant de récupérer tous les "num" et "libelle" de la table "classe"

								$sclasses8 = $conn->query("SELECT num, libelle FROM stage.classe;");
								while($sliste_classes8 = $sclasses8->fetch()){
									if (isset($stabT8[$sliste_classes8['num']])) {
?>
										<option value=<?php echo "".$sliste_classes8['num']?> <?php echo "selected"; ?>> <?php echo $sliste_classes8['libelle'] ;?></option>
<?php
									}
									else {
?>
										<option value=<?php echo "".$sliste_classes8['num']?>> <?php echo $sliste_classes8['libelle'] ;?></option>
<?php
									}
								}
?>
							</select>
<?php

							//requête permettant de récupérer le cours et la proposition de la table "remplacement" en fonction du "num" de la sortie sélectionnée

							$srequete_remplacement8 = $conn->query("SELECT cours, proposition FROM stage.remplacement WHERE sortie =".$_SESSION['modification']." ORDER BY num asc limit 7,1;");
							$sdonnees8 = $srequete_remplacement8->fetch();
?>
							&nbsp;&nbsp; <input type="text" name="Cours8" id="Cours8" value="<?php echo $sdonnees8['cours'] ?>" >
							&nbsp;&nbsp; <input type="text" name="proposition8" id="proposition8" value="<?php echo $sdonnees8['proposition'] ?>" >
							<br>
							<br>
						</div>
						<hr style="border-color: green">
<?php

						//requête permettant de tout récupérer de la table "repas" en fonction du "num" de la sortie sélectionnée

						$requeteP2 = $conn->query("SELECT repas.num, type, vegetarien, nbVegetarien, nbDP, nbInterne, nbExterne, heureEnlevement FROM stage.sortie INNER JOIN stage.repas ON stage.sortie.num=stage.repas.sortie WHERE sortie=".$_SESSION['modification'].";");
						$SR = $requeteP2->fetch();
?>
						<b>Choix du repas :</b>
						<br>
						&nbsp; &nbsp; &nbsp;
						<div class="row">
							&nbsp; &nbsp; &nbsp; &nbsp;
							<input type="radio" name="Repas" id="RepasNon" value="Pas de Repas" <?php if( $SR['type'] == ""){  echo "checked"; }?>>
							<label for="RepasNon"><i>Pas de repas</i></label>
							&nbsp; &nbsp; &nbsp;
							<input type="radio" name="Repas" id="RepasSecs" value="Repas Secs" <?php if( $SR['type'] == "Repas Secs"){  echo "checked"; }?>>
							<label for="RepasSecs"><i>Repas Secs (</i>à utiliser pour déplacements, <i><b>pas de conservation au froid)</b></i></label>
							&nbsp; &nbsp; &nbsp;
							<input type="radio" name="Repas" id="RepasRefrigeres" value="Repas Réfrigérés" <?php if( $SR['type'] == "Repas Réfrigérés"){  echo "checked"; }?>>
							<label for="RepasRefrigeres"><i>Repas réfrigérés (</i>repas complet <i><b>tenus au frais en glacière)</b></i></label>
						</div>
						<br>
						<hr>
						<div class="table-responsive bg-light">
							<br>
							<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Repas Végétariens" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre de repas " ?> "><br><br>
							<select name="vegetarien" id="vegetarien" >
							<option selected disabled hidden>Choisissez ici</option>
<?php
						if ($SR['vegetarien'] == "Oui") {
?>
							<option value="Oui" selected >Oui</option>
							<option value="Non">Non</option>
<?php
						}
						else if ($SR['vegetarien'] == "Non") {
?>
							<option value="Oui">Oui</option>
							<option value="Non" selected >Non</option>
<?php
						}
						else {
?>
							<option value="Oui">Oui</option>
							<option value="Non">Non</option>
<?php
						}
?>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="number" min="0" name="nbVegetarien" id="nbVegetarien" value="<?php echo $SR['nbVegetarien'] ?>"><br><br>
						</div>
						<br>
						<br>
						<b>Nombre total de repas à réserver : </b>
						<br>
						<hr>
						<br>
						<b>Apprenants :</b>
						<br>
						<br>
						<div class="row">
							<div class="form-group col text-center">
								<label for="nbInterne"><b>Interne(s) : </b></label>
								<input class="form-control" id="nbInterne" name="nbInterne" type="number" min="0" value="<?php echo $SR['nbInterne'] ?>" />
							</div>
							&nbsp; &nbsp; &nbsp; &nbsp;
							<div class="form-group col text-center">
								<label for="nbDP"><b>Demi-Pensionnaire(s) : </b></label>
								<input class="form-control" id="nbDP" name="nbDP" type="number" min="0" value="<?php echo $SR['nbDP'] ?>" />
							</div>
							&nbsp; &nbsp; &nbsp; &nbsp;
							<div class="form-group col text-center">
								<label for="nbExterne"><b>Externe(s) : </b></label>
								<input class="form-control" id="nbExterne" name="nbExterne" type="number" min="0" value="<?php echo $SR['nbExterne'] ?>" />
							</div>
						</div>
						<hr>
						<label for="heureEnlevement"><b>Heure d'enlèvement en cuisine : </b>
						<input class="form-control" id="heureEnlevement" name="heureEnlevement" type="time" value="<?php echo $SR['heureEnlevement'] ?>" /></label>
						<hr style="border-color: green">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
							</div>
						</div>
					</div>
				</form>
<?php
				}
			}
?>
		</div>
	</div>
<?php
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	}
</style>
