<?php
session_start();

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$nbInterne = $_POST['nbInterne'];
$nbDP = $_POST['nbDP'];
$nbExterne = $_POST['nbExterne'];
$verificationtotal = $nbInterne + $nbDP + $nbExterne;
$nbEleves = $_POST['nbEleves'];
$typeRepas = $_POST['Repas'];
$heureDepart = $_POST['heureDepart'];
$heureRetour = $_POST['heureRetour'];
$heureEnlevement = $_POST['heureEnlevement'];
$dateSortie = $_POST['dateSortie'];
$dateDemande = date("Y-m-d");
$vegetarien = $_POST['vegetarien'];
$nbVegetarien = $_POST['nbVegetarien'];
$VCA = 0;
if (isset($_POST['utilisateurs'])) {
	$listeVCA = $_POST['utilisateurs'];

	//parcours la liste des cases cochées pour les accompagnateurs

	foreach ($listeVCA as $accompagneVCU) {
		$VCA += 1;
	}
}
$verificationtotalRV = $VCA + $nbInterne + $nbDP + $nbExterne;

if(($typeRepas != "Pas de Repas" && $vegetarien == "Oui" && $verificationtotalRV < $nbVegetarien) || ($typeRepas != "Pas de Repas" && $verificationtotal != $nbEleves) || ($typeRepas != "Pas de Repas" && $heureEnlevement == "") || ($heureDepart >= $heureRetour) || ($dateDemande >= $dateSortie) || (empty($_POST['utilisateurs'])) || (empty($_POST['classes']))) {

	unset($_SESSION['natureVe']);
	unset($_SESSION['dateSortieVe']);
	unset($_SESSION['heureDepartVe']);
	unset($_SESSION['heureRetourVe']);
	unset($_SESSION['destinationVe']);
	unset($_SESSION['NbKmVe']);
	unset($_SESSION['lienReferentielVe']);
	unset($_SESSION['nbTicketSetramVe']);
	unset($_SESSION['nbElevesVe']);
	unset($_SESSION['responsableVe']);
	unset($_SESSION['Classei1Ve']);
	unset($_SESSION['nbi1Ve']);
	unset($_SESSION['centrei1Ve']);
	unset($_SESSION['Classei2Ve']);
	unset($_SESSION['nbi2Ve']);
	unset($_SESSION['centrei2Ve']);
	unset($_SESSION['utilisateursVe']);
	unset($_SESSION['vehiculesVe']);
	unset($_SESSION['classesVe']);
	unset($_SESSION['Cours1Ve']);
	unset($_SESSION['proposition1Ve']);
	unset($_SESSION['classe1Ve']);
	unset($_SESSION['Cours2Ve']);
	unset($_SESSION['proposition2Ve']);
	unset($_SESSION['classe2Ve']);
	unset($_SESSION['Cours3Ve']);
	unset($_SESSION['proposition3Ve']);
	unset($_SESSION['classe3Ve']);
	unset($_SESSION['Cours4Ve']);
	unset($_SESSION['proposition4Ve']);
	unset($_SESSION['classe4Ve']);
	unset($_SESSION['Cours5Ve']);
	unset($_SESSION['proposition5Ve']);
	unset($_SESSION['classe5Ve']);
	unset($_SESSION['Cours6Ve']);
	unset($_SESSION['proposition6Ve']);
	unset($_SESSION['classe6Ve']);
	unset($_SESSION['Cours7Ve']);
	unset($_SESSION['proposition7Ve']);
	unset($_SESSION['classe7Ve']);
	unset($_SESSION['Cours8Ve']);
	unset($_SESSION['proposition8Ve']);
	unset($_SESSION['classe8Ve']);
	unset($_SESSION['RepasVe']);
	unset($_SESSION['nbInterneVe']);
	unset($_SESSION['nbDPVe']);
	unset($_SESSION['nbExterneVe']);
	unset($_SESSION['heureEnlevementVe']);
	unset($_SESSION['vegetarienVe']);
	unset($_SESSION['nbVegetarienVe']);
	unset($_SESSION['verification1']);
	unset($_SESSION['verification2']);
	unset($_SESSION['verification3']);
	unset($_SESSION['verification4']);
	unset($_SESSION['verification5']);
	unset($_SESSION['verification6']);
	unset($_SESSION['verification7']);

	$_SESSION['demande_Vea'] = 1;
	$_SESSION['natureVe'] = $_POST['nature'];
	$_SESSION['dateSortieVe'] = $_POST['dateSortie'];
	$_SESSION['heureDepartVe'] = $_POST['heureDepart'];
	$_SESSION['heureRetourVe'] = $_POST['heureRetour'];
	$_SESSION['destinationVe'] = $_POST['destination'];
	$_SESSION['NbKmVe'] = $_POST['NbKm'];
	$_SESSION['lienReferentielVe'] = $_POST['lienReferentiel'];
	$_SESSION['nbTicketSetramVe'] = $_POST['nbTicketSetram'];
	$_SESSION['nbElevesVe'] = $_POST['nbEleves'];
	$_SESSION['responsableVe'] = $_POST['responsable'];
	$_SESSION['Classei1Ve'] = $_POST['Classei1'];
	$_SESSION['nbi1Ve'] = $_POST['nbi1'];
	$_SESSION['centrei1Ve'] = $_POST['centrei1'];
	$_SESSION['Classei2Ve'] = $_POST['Classei2'];
	$_SESSION['nbi2Ve'] = $_POST['nbi2'];
	$_SESSION['centrei2Ve'] = $_POST['centrei2'];
	$_SESSION['utilisateursVe'] = $_POST['utilisateurs'];
	$_SESSION['vehiculesVe'] = $_POST['vehicules'];
	$_SESSION['classesVe'] = $_POST['classes'];
	$_SESSION['Cours1Ve'] = $_POST['Cours1'];
	$_SESSION['proposition1Ve'] = $_POST['proposition1'];
	$_SESSION['classe1Ve'] = $_POST['classe1'];
	$_SESSION['Cours2Ve'] = $_POST['Cours2'];
	$_SESSION['proposition2Ve'] = $_POST['proposition2'];
	$_SESSION['classe2Ve'] = $_POST['classe2'];
	$_SESSION['Cours3Ve'] = $_POST['Cours3'];
	$_SESSION['proposition3Ve'] = $_POST['proposition3'];
	$_SESSION['classe3Ve'] = $_POST['classe3'];
	$_SESSION['Cours4Ve'] = $_POST['Cours4'];
	$_SESSION['proposition4Ve'] = $_POST['proposition4'];
	$_SESSION['classe4Ve'] = $_POST['classe4'];
	$_SESSION['Cours5Ve'] = $_POST['Cours5'];
	$_SESSION['proposition5Ve'] = $_POST['proposition5'];
	$_SESSION['classe5Ve'] = $_POST['classe5'];
	$_SESSION['Cours6Ve'] = $_POST['Cours6'];
	$_SESSION['proposition6Ve'] = $_POST['proposition6'];
	$_SESSION['classe6Ve'] = $_POST['classe6'];
	$_SESSION['Cours7Ve'] = $_POST['Cours7'];
	$_SESSION['proposition7Ve'] = $_POST['proposition7'];
	$_SESSION['classe7Ve'] = $_POST['classe7'];
	$_SESSION['Cours8Ve'] = $_POST['Cours8'];
	$_SESSION['proposition8Ve'] = $_POST['proposition8'];
	$_SESSION['classe8Ve'] = $_POST['classe8'];
	$_SESSION['RepasVe'] = $_POST['Repas'];
	$_SESSION['nbInterneVe'] = $_POST['nbInterne'];
	$_SESSION['nbDPVe'] = $_POST['nbDP'];
	$_SESSION['nbExterneVe'] = $_POST['nbExterne'];
	$_SESSION['heureEnlevementVe'] = $_POST['heureEnlevement'];
	$_SESSION['vegetarienVe'] = $_POST['vegetarien'];
	$_SESSION['nbVegetarienVe'] = $_POST['nbVegetarien'];

	if ($dateDemande >= $dateSortie){
		$_SESSION['verification1'] = 1;
	}
	if ($heureDepart >= $heureRetour){
		$_SESSION['verification2'] = 1;
	}
	if (empty($_POST['classes'])) {
		$_SESSION['verification3'] = 1;
	}
	if (empty($_POST['utilisateurs'])) {
		$_SESSION['verification4'] = 1;
	}
	if ($typeRepas != "Pas de Repas" && $verificationtotal != $nbEleves){
		$_SESSION['verification5'] = 1;
	}
	if ($typeRepas != "Pas de Repas" && $heureEnlevement == ""){
		$_SESSION['verification6'] = 1;
	}
	if ($typeRepas != "Pas de Repas" && $vegetarien == "Oui" && $verificationtotalRV < $nbVegetarien){
		$_SESSION['verification7'] = 1;
	}
	
	header("Location: ajouter.php");
}
else {
	//récupération de toutes les données saisis dans le formulaire d'ajout
	
	$_SESSION['demande_Vea'] = 0;
	$nature = $_POST['nature'];
	$destination = $_POST['destination'];
	$NbKm = $_POST['NbKm'];
	$lienReferentiel = $_POST['lienReferentiel'];
	$nbTicketSetram = $_POST['nbTicketSetram'];
	$connecte = $_SESSION['id'];
	$responsable = $_POST['responsable'];

	//requête d'insertion de données dans la table "sortie"

	$sortie = ("INSERT INTO stage.sortie(destination, nature, dateSortie, heureDepart, heureRetour, nbKm, lienReferentiel, nbTicketSetram, nbEleves, dateDemande, connecte, responsable) VALUES('" . $destination . "','" . $nature . "','" . $dateSortie . "','" . $heureDepart . "','" . $heureRetour . "','" . $NbKm . "','" . $lienReferentiel . "','" . $nbTicketSetram . "','" . $nbEleves . "','" . $dateDemande . "','" . $connecte . "','" . $responsable . "');");
	$conn->exec($sortie);

	//requête de sélection du numéro correspondant à la requête précédente

	$requete = $conn->query("SELECT num FROM stage.sortie WHERE destination = '" . $destination . "' AND nature = '" . $nature . "' AND dateSortie = '" . $dateSortie . "' AND heureDepart = '" . $heureDepart . "' AND heureRetour = '" . $heureRetour . "' AND nbKm = '" . $NbKm . "' AND lienReferentiel = '" . $lienReferentiel . "' AND nbTicketSetram = '" . $nbTicketSetram . "' AND nbEleves = '" . $nbEleves . "' AND dateDemande = '" . $dateDemande . "' AND connecte = '" . $connecte . "' AND responsable = '" . $responsable . "';");
	$numero = $requete->fetch();
	$num = $numero['num'];

	//requête d'insertion de données dans la table "organise" en fonction du "num" trouvé précedemment

	$organise = ("INSERT INTO stage.organise VALUES ('" . $num . "',1);");
	$conn->exec($organise);

	//vérifie si il y a au moins une classe de renseigné pour les invites

	if (!empty($_POST['Classei1'])) {
			
		//vérifie si une valeur non nulle existe pour une classe
			
		if ($_POST['Classei1'] != "") {
		
			//requête d'insertion de données dans la table "invite" en fonction du "num" de la sortie trouvé précedemment
			
			$requete6 = ("INSERT INTO stage.invite (classe, nombre, sortie, centre) VALUES('" . $_POST['Classei1'] . "','" . $_POST['nbi1'] . "','" . $num . "','" . $_POST['centrei1'] . "');");
			$conn->exec($requete6);
		}
		if ($_POST['Classei2'] != "") {
		
			//requête d'insertion de données dans la table "invite" en fonction du "num" de la sortie trouvé précedemment
			
			$requete7 = ("INSERT INTO stage.invite (classe, nombre, sortie, centre) VALUES('" . $_POST['Classei2'] . "','" . $_POST['nbi2'] . "','" . $num . "','" . $_POST['centrei2'] . "');");
			$conn->exec($requete7);
		}
	}

	//vérifie si il y a au moins une case de cochée dans le formulaire pour les accompagnateurs

	if (isset($_POST['utilisateurs'])) {
		$listeA = $_POST['utilisateurs'];
		
		//parcours la liste des cases cochées pour les accompagnateurs

		foreach ($listeA as $accompagneU) {
			
			//requête d'insertion de données dans la table "accompagne" en fonction du "num" trouvé précedemment

			$requete3 = ("INSERT INTO stage.accompagne VALUES('" . $num . "','" . $accompagneU . "');");
			$conn->exec($requete3);
		}
	}

	//vérifie si il y a au moins une case de cochée dans le formulaire pour les véhicules

	if (isset($_POST['vehicules'])) {
		$listeV = $_POST['vehicules'];
		
		//parcours la liste des cases cochées pour les véhicules

		foreach ($listeV as $utiliseV) {
			
			//requête d'insertion de données dans la table "utilise" en fonction du "num" trouvé précedemment
			
			$requete1 = ("INSERT INTO stage.utilise VALUES('" . $num . "','" . $utiliseV . "');");
			$conn->exec($requete1);
		}
	}

	//vérifie si il y a au moins une case de cochée dans le formulaire pour les classes

	if (isset($_POST['classes'])) {
		$listeC = $_POST['classes'];
		
		//parcours la liste des cases cochées pour les classes

		foreach ($listeC as $concerneC) {
			
			//requête d'insertion de données dans la table "concerne" en fonction du "num" de la sortie trouvé précedemment
			
			$requete2 = ("INSERT INTO stage.concerne VALUES('" . $num . "','" . $concerneC . "');");
			$conn->exec($requete2);
		}
	}

	//vérifie si il y a au moins un cours de renseigné pour les remplacements

	if (!empty($_POST['Cours1'])) {
		for ($i = 1; $i < 9; $i++) {
			
			//vérifie si une valeur non nulle existe pour un cours
			
			if ($_POST['Cours' . $i] != "") {
				
				//requête d'insertion de données dans la table "remplacement" en fonction du "num" de la sortie trouvé précedemment
				
				$requete4 = ("INSERT INTO stage.remplacement (cours, proposition, sortie, classe) VALUES('" . $_POST['Cours' . $i] . "','" . $_POST['proposition' . $i] . "','" . $num . "','" . $_POST['classe' . $i] . "');");
				$conn->exec($requete4);
			}
		}
	}

	//vérifie si il y a au moins un type de repas

	if($typeRepas != "Pas de Repas") {
		
		//requête mettant à jour la reservation en fonction du "num" de la sortie trouvé précedemment
		
		$sortie1 = ("UPDATE stage.sortie SET reservation = 1 WHERE num = '" . $num . "';");
		$conn->exec($sortie1);
		
		$requeteNB = $conn->query("SELECT (COUNT(*)) AS nba FROM stage.accompagne WHERE sortie = '" . $num . "';");
		$numeroNB = $requeteNB->fetch();
		$nbAccompagnateurs = $numeroNB['nba'];
		
		//requête d'insertion de données dans la table "repas" en fonction du "num" de la sortie trouvé précedemment
		
		$requete5 = ("INSERT INTO stage.repas (type, nbDP, nbInterne, nbExterne, nbAccompagnateurs, heureEnlevement, sortie) VALUES('" . $typeRepas . "','" . $nbDP . "','" . $nbInterne . "','" . $nbExterne . "','" . $nbAccompagnateurs . "','" . $heureEnlevement . "','" . $num . "');");
		$conn->exec($requete5);
		
		if($vegetarien == "Oui") {
			$vegetarien1 = ("UPDATE stage.repas SET vegetarien = '" . $vegetarien . "', nbVegetarien = '" . $nbVegetarien . "' WHERE sortie = '" . $num . "';");
			$conn->exec($vegetarien1);
		}	
	}

	//envoie de mails automatiques aux utilisateurs dont leur statut est "Proviseur Adjoint"

	$mailH = $conn->query("SELECT prenom, nom, mail FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte WHERE sortie.num = '" . $num . "';");
	$donnees_mailH = $mailH->fetch();
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 2;");

	$sujet = "Nouvelle demande de sortie";
	$message = "Bonjour, \n ".$donnees_mailH['prenom']." ".$donnees_mailH['nom']." vient de réaliser une demande de sortie pour le ".$dateSortie." de ".$heureDepart." à ".$heureRetour.". \n\n Si vous souhaitez visualiser cette demande, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
	$headers = 'From: ' .$donnees_mailH['mail']."\r\n"."Content-Type: text/html; charset=utf-8 ";

	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}

	//permet de se diriger vers la page "tableau.php"

	header("Location: tableau.php");
}
?>
