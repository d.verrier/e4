<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$num = $_POST['precisionProviseur'];
$precisionP = ("UPDATE stage.sortie SET precisionProviseur ='".$_POST['precision']."', proviseur = '".$_SESSION['id']."' WHERE num = '".$num."';");
$conn -> exec($precisionP);

$mailH = $conn->query("SELECT mail, dateSortie, heureDepart, heureRetour, precisionProviseur FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte WHERE sortie.num = '" . $num . "';");
$donnees_mailH = $mailH->fetch();
$mailD = $conn->query("SELECT prenom, nom, mail FROM stage.utilisateur WHERE id = '".$_SESSION['id']."';");
$donnees_mailD = $mailD->fetch();
$sujet = "Demande de précision pour une sortie";
$message = "Bonjour, \n ".$donnees_mailD['prenom']." ".$donnees_mailD['nom']." vous demande de modifier la demande de sortie que vous avez fait pour le ".$donnees_mailH['dateSortie']." de ".$donnees_mailH['heureDepart']." à ".$donnees_mailH['heureRetour'].". La précision que vous devez faire est : ".$donnees_mailH['precisionProviseur'].". \n\n Si vous souhaitez visualiser cette demande, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
$headers = 'From: ' .$donnees_mailD['mail']."\r\n"."Content-Type: text/html; charset=utf-8 ";
$destinataire = $donnees_mailH['mail'];

mail($destinataire, $sujet, $message, $headers);

header("Location: ../tableau.php");
?>
