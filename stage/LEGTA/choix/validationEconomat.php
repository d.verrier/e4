<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$num = $_POST['validationEconomat'];
$validation = ("UPDATE stage.sortie SET validationEconomat = 1, economat = '".$_SESSION['id']."' WHERE num = '".$num."';");
$conn -> exec($validation);

$mailH = $conn->query("SELECT prenom, nom, mail, dateSortie, heureDepart, heureRetour FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte WHERE sortie.num = ".$num.";");
$donnees_mailH = $mailH->fetch();

$sujet = "Nouvelle demande de sortie";
$message = "Bonjour, \n ".$donnees_mailH['prenom']." ".$donnees_mailH['nom']." a réalisé une demande de sortie pour le ".$donnees_mailH['dateSortie']." de ".$donnees_mailH['heureDepart']." à ".$donnees_mailH['heureRetour'].". \n\n Si vous souhaitez visualiser cette demande, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
$headers = 'From: ' .$donnees_mailH['mail']."\r\n"."Content-Type: text/html; charset=utf-8 ";
$mailD1 = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 3;");

while ($donnees_mailD1 = $mailD1->fetch()){
	$destinataire = $donnees_mailD1['mail'];

	mail($destinataire, $sujet, $message, $headers);
}

header("Location: ../tableau.php");
?>
