<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$num = $_POST['validationVieScolaire'];
$validation = ("UPDATE stage.sortie SET validationVieScolaire = 1, vieScolaire = '".$_SESSION['id']."' WHERE num = '".$num."';");
$conn -> exec($validation);

$validationVS = $conn->query("SELECT reservation FROM stage.sortie where num = ".$num.";");
$statut = $validationVS->fetch();

$mailH = $conn->query("SELECT prenom, nom, mail, dateSortie, heureDepart, heureRetour FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte WHERE sortie.num = ".$num.";");
$donnees_mailH = $mailH->fetch();

$mailHe = $conn->query("SELECT mail FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.vieScolaire WHERE sortie.num = ".$num.";");
$donnees_mailHe = $mailHe->fetch();

$sujet = "Demande de sortie validée";
$headers = 'From: ' .$donnees_mailHe['mail']."\r\n"."Content-Type: text/html; charset=utf-8 ";

if($statut['reservation']==1){

	$message = "Bonjour, \n ".$donnees_mailH['prenom']." ".$donnees_mailH['nom']." a réalisé une demande de sortie qui a été validée par le proviseur adjoint, l'economat et la vie scolaire pour le ".$donnees_mailH['dateSortie']." de ".$donnees_mailH['heureDepart']." à ".$donnees_mailH['heureRetour'].". \n\n Si vous souhaitez visualiser cette demande, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
	$mailDe = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 4;");
	
	while ($donnees_mailDe = $mailDe->fetch()){
		$destinataire = $donnees_mailDe['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 2;");
	
	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
	$destinataire = $donnees_mailH['mail'];
	mail($destinataire, $sujet, $message, $headers);
}

else {

	$message = "Bonjour, \n ".$donnees_mailH['prenom']." ".$donnees_mailH['nom']." a réalisé une demande de sortie qui a été validée par le proviseur adjoint et la vie scolaire pour le ".$donnees_mailH['dateSortie']." de ".$donnees_mailH['heureDepart']." à ".$donnees_mailH['heureRetour'].". \n\n Si vous souhaitez visualiser cette demande, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 2;");
	
	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
	$destinataire = $donnees_mailH['mail'];
	mail($destinataire, $sujet, $message, $headers);
	
}

header("Location: ../tableau.php");
?>
