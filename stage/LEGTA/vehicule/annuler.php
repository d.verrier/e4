<?php
session_start();
include '../entete.php';
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");
if ($_SESSION['statut']==7) {
?>
	<div class="container-fluid" align="center">
		<br>
		<br>
		<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #a60808; border-right: 5px solid #a60808">
			<br>
			<h1 style="font-family: 'Gentium Book Basic'">Supprimer un vehicule :</h1>
			<br>
		</div>
		<br>
		<div class="tab-pane fade active show">
			<form class="form-horizontal" method="post" action="suppression.php">
				<div class="alert alert-secondary">
					<br>
					<p align="center"> Choisissez le vehicule à supprimer :
						<select id="suppression" name="suppression">
<?php
						$demande = $conn->query("SELECT num, immatriculation, modele FROM stage.vehicule WHERE immatriculation != 'Bus';");
						while($liste_demande = $demande->fetch()){
?>
							<option value=<?php echo "".$liste_demande['num']?>> <?php echo $liste_demande['immatriculation']," ",$liste_demande['modele'] ;?></option>
<?php
						}
?>
						</select>
						<button type="submit" class="btn btn-info">Valider</button>
					</p>
				</div>
			</form>
		</div>
	</div>
<?php
}

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="../connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	}
</style>
