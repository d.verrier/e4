<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$immatriculation = strtoupper($_POST['immatriculation']);
$immatriculation = strtr ($immatriculation ,"-" ," ");
$marque = $_POST['marque'];
$modele = $_POST['modele'];
$nbPlaces = $_POST['nbPlaces'];
$info = $_POST['info'];

$verification = $conn->query("SELECT count(*) AS verification FROM stage.vehicule WHERE immatriculation = '" . $immatriculation . "';");
$existe = $verification->fetch();

if ($existe['verification']>=1){
	unset($_SESSION['immatriculationEx']);
	unset($_SESSION['marqueEx']);
	unset($_SESSION['modeleEx']);
	unset($_SESSION['nbPlacesEx']);
	unset($_SESSION['infoEx']);

	$_SESSION['existeav']=1;
	$_SESSION['immatriculationEx'] = $_POST['immatriculation'];
	$_SESSION['marqueEx'] = $_POST['marque'];
	$_SESSION['modeleEx'] = $_POST['modele'];
	$_SESSION['nbPlacesEx'] = $_POST['nbPlaces'];
	$_SESSION['infoEx'] = $_POST['info'];

	header("Location: ajouter.php");
}
else {
	$_SESSION['existeav']=0;
	$vehicule = ("INSERT INTO stage.vehicule(immatriculation, marque, modele, nbplaces, info) VALUES('" . $immatriculation . "','" . $marque . "','" . $modele . "','" . $nbPlaces . "','" . $info . "');");
	$conn->exec($vehicule);

	header("Location: tableau.php");
}
?>
