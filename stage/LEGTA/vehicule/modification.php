<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$immatriculation = strtoupper($_POST['immatriculation']);
$immatriculation = strtr ($immatriculation ,"-" ," ");
$marque = $_POST['marque'];
$modele = $_POST['modele'];
$nbPlaces = $_POST['nbPlaces'];
$info = $_POST['info'];

$verification = $conn->query("SELECT count(*) AS verification FROM stage.vehicule WHERE immatriculation = '" . $immatriculation . "';");
$existe = $verification->fetch();

$verificationv = $conn->query("SELECT immatriculation FROM stage.vehicule WHERE num = ".$_SESSION['modification'].";");
$existev = $verificationv->fetch();
if ((($existe['verification']>=1) && ($existev['immatriculation'] != $immatriculation)) || (($existe['verification']>=2) && ($existev['immatriculation'] == $immatriculation))){

	unset($_SESSION['immatriculationEx']);
	unset($_SESSION['marqueEx']);
	unset($_SESSION['modeleEx']);
	unset($_SESSION['nbPlacesEx']);
	unset($_SESSION['infoEx']);

	$_SESSION['existemv']=1;
	$_SESSION['immatriculationEx'] = $_POST['immatriculation'];
	$_SESSION['marqueEx'] = $_POST['marque'];
	$_SESSION['modeleEx'] = $_POST['modele'];
	$_SESSION['nbPlacesEx'] = $_POST['nbPlaces'];
	$_SESSION['infoEx'] = $_POST['info'];

	header("Location: modifier.php");
}
else {
	$_SESSION['existemv']=0;
	$_SESSION['demande_modificationv'] = 0;
	$vehicule = ("UPDATE stage.vehicule SET immatriculation='" . $immatriculation . "', marque='" . $marque . "', modele='" . $modele . "', nbplaces='" . $nbPlaces . "', info='" . $info . "' WHERE num = ".$_SESSION['modification'].";");
	$conn->exec($vehicule);

	header("Location: tableau.php");
}
?>
