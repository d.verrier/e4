<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//permet de vérifier que l'utilisateur connecté à un statut "enseignant" ou "non-enseignant"

if ($_SESSION['statut']==1 || $_SESSION['statut']==6) {
?>
	<div class="container-fluid" align="center">
		<br>
		<br>
		<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #a60808; border-right: 5px solid #a60808">
			<br>
			<h1 style="font-family: 'Gentium Book Basic'">Formulaire de demande :</h1>
			<br>
		</div>
		<br>
		<div class="tab-pane fade active show">
			<form class="form-horizontal" method="post" action="suppression.php">
				<div class="alert alert-secondary">
					<br>
					<p align="center"> Choisissez la demande à annuler :
						<select id="suppression" name="suppression">
<?php

						//requête de sélection permettant à l'utilisateur de choisir parmi ses demandes rangé par "dateSortie décroissante"  celle qui souhaite supprimer

						$demande = $conn->query("SELECT num, destination, dateSortie, heureDepart, heureRetour FROM stage.sortie INNER JOIN stage.organise ON stage.sortie.num=stage.organise.sortie INNER JOIN stage.utilisateur ON stage.sortie.connecte=stage.utilisateur.id WHERE organise.centre = 1 AND sortie.connecte='".$_SESSION['id']."' ORDER BY dateSortie desc;");
						while($liste_demande = $demande->fetch()){
?>
							<option value=<?php echo "".$liste_demande['num']?>> <?php echo $liste_demande['destination']," le ",$liste_demande['dateSortie']," de ",$liste_demande['heureDepart']," à ",$liste_demande['heureRetour'] ;?></option>
<?php
						}
?>
						</select>
						<button type="submit" class="btn btn-info">Valider</button>
					</p>
				</div>
			</form>
		</div>
	</div>
<?php
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>
