<?php
session_start();

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//permet de réinitialiser toutes les variables SESSION

unset($_SESSION['pdfnum']);
unset($_SESSION['pdfnature']);
unset($_SESSION['pdfdateSortie']);
unset($_SESSION['pdfheureDepart']);
unset($_SESSION['pdfheureRetour']);
unset($_SESSION['pdfnbKm']);
unset($_SESSION['pdfcoutEstimatif']);
unset($_SESSION['pdflienReferentiel']);
unset($_SESSION['pdfnbTicketSetram']);
unset($_SESSION['pdfnbEleves']);
unset($_SESSION['pdfdateDemande']);
unset($_SESSION['pdfconnecteprenom']);
unset($_SESSION['pdfconnectenom']);
unset($_SESSION['pdfconnectetelephone']);
unset($_SESSION['pdfconnectemail']);
unset($_SESSION['pdfresponsableprenom']);
unset($_SESSION['pdfresponsablenom']);
unset($_SESSION['pdfresponsabletelephone']);
unset($_SESSION['pdfresponsablemail']);
unset($_SESSION['pdftype']);
unset($_SESSION['pdfvegetarien']);
unset($_SESSION['pdfnbVegetarien']);
unset($_SESSION['pdfnbDP']);
unset($_SESSION['pdfnbInterne']);
unset($_SESSION['pdfnbExterne']);
unset($_SESSION['pdfnbAccompagnateurs']);
unset($_SESSION['pdfheureEnlevement']);
unset($_SESSION['pdfvehiculeimmatriculation']);
unset($_SESSION['pdfvehiculemodele']);
unset($_SESSION['pdfvehicule']);
unset($_SESSION['pdfclasselibelle']);
unset($_SESSION['pdfclasse']);
unset($_SESSION['pdfutilisateurprenom']);
unset($_SESSION['pdfutilisateurnom']);
unset($_SESSION['pdfutilisateur']);
unset($_SESSION['pdfremplacementlibelle']);
unset($_SESSION['pdfremplacementcours']);
unset($_SESSION['pdfremplacementproposition']);
unset($_SESSION['pdfremplacement']);
unset($_SESSION['pdfinviteclasse']);
unset($_SESSION['pdfinvitenombre']);
unset($_SESSION['pdfinvitecentre']);
unset($_SESSION['pdfinvite']);
unset($_SESSION['pdfconnectestatut']);
unset($_SESSION['pdfresponsablestatut']);

//permet de récupérer toutes les valeurs dans des variables SESSION suivant le "num" de la sortie sélectionnée

$_SESSION['pdfnum'] = $_POST['num_pdf'];

//requête permettant de récupérer toutes les valeurs de la table "sortie" en fonction de son "num"

$pdf1 = $conn->query("SELECT * FROM stage.sortie WHERE sortie.num='".$_SESSION['pdfnum']."';");
$liste_pdf = $pdf1->fetch();
$_SESSION['pdfdestination'] = $liste_pdf['destination'];
$_SESSION['pdfnature'] = $liste_pdf['nature'];

list($year, $month, $day) = explode("-", $liste_pdf['dateSortie']);
$tdateso = $day.'/'.$month.'/'.$year;
$_SESSION['pdfdateSortie'] = $tdateso;

$_SESSION['pdfheureDepart'] = $liste_pdf['heureDepart'];
$_SESSION['pdfheureRetour'] = $liste_pdf['heureRetour'];
$_SESSION['pdfnbKm'] = $liste_pdf['nbKm'];
$_SESSION['pdfcoutEstimatif'] = $liste_pdf['coutEstimatif'];
$_SESSION['pdflienReferentiel'] = $liste_pdf['lienReferentiel'];
$_SESSION['pdfnbTicketSetram'] = $liste_pdf['nbTicketSetram'];
$_SESSION['pdfnbEleves'] = $liste_pdf['nbEleves'];

list($year, $month, $day) = explode("-", $liste_pdf['dateDemande']);
$tdateso = $day.'/'.$month.'/'.$year;
$_SESSION['pdfdateDemande'] = $tdateso;

//requête permettant de récupérer toutes les valeurs de la table "utilisateur" en fonction du "num" de sortie pour connaitre les informations du demandeur de sortie

$pdf3 = $conn->query("SELECT prenom, nom, telephone, mail, libelle FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte INNER JOIN stage.statut ON stage.utilisateur.statut = stage.statut.num WHERE sortie.num='".$_SESSION['pdfnum']."';");
$liste_pdf2 = $pdf3->fetch();
$_SESSION['pdfconnecteprenom'] = $liste_pdf2['prenom'];
$_SESSION['pdfconnectenom'] = $liste_pdf2['nom'];
$_SESSION['pdfconnectetelephone'] = $liste_pdf2['telephone'];
$_SESSION['pdfconnectemail'] = $liste_pdf2['mail'];
$_SESSION['pdfconnectestatut'] = $liste_pdf2['libelle'];

//requête permettant de récupérer toutes les valeurs de la table "utilisateur" en fonction du "num" de sortie pour connaitre les informations du responsable de sortie

$pdf4 = $conn->query("SELECT prenom, nom, telephone, mail, libelle FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.responsable INNER JOIN stage.statut ON stage.utilisateur.statut = stage.statut.num WHERE sortie.num='".$_SESSION['pdfnum']."';");
$liste_pdf3 = $pdf4->fetch();
$_SESSION['pdfresponsableprenom'] = $liste_pdf3['prenom'];
$_SESSION['pdfresponsablenom'] = $liste_pdf3['nom'];
$_SESSION['pdfresponsabletelephone'] = $liste_pdf3['telephone'];
$_SESSION['pdfresponsablemail'] = $liste_pdf3['mail'];
$_SESSION['pdfresponsablestatut'] = $liste_pdf2['libelle'];

//permet de vérifier qu'il y ai bien des repas en fonction du "num" de sortie

if ($liste_pdf['reservation'] == 1){

	//requête permettant de récupérer toutes les valeurs de la table "repas" en fonction du "num" de sortie

	$pdf2 = $conn->query("SELECT * FROM stage.repas WHERE sortie='".$_SESSION['pdfnum']."';");
	$liste_pdf1 = $pdf2->fetch();
	$_SESSION['pdftype'] = $liste_pdf1['type'];
	$_SESSION['pdfvegetarien'] = $liste_pdf1['vegetarien'];
	$_SESSION['pdfnbVegetarien'] = $liste_pdf1['nbVegetarien'];
	$_SESSION['pdfnbDP'] = $liste_pdf1['nbDP'];
	$_SESSION['pdfnbInterne'] = $liste_pdf1['nbInterne'];
	$_SESSION['pdfnbExterne'] = $liste_pdf1['nbExterne'];
	$_SESSION['pdfnbAccompagnateurs'] = $liste_pdf1['nbAccompagnateurs'];
	$_SESSION['pdfheureEnlevement'] = $liste_pdf1['heureEnlevement'];
}

//requête permettant de récupérer toutes les immatriculations et modèles de la table "vehicule" en fonction du "num" de sortie

$pdf5 = $conn->query("SELECT immatriculation, modele FROM stage.vehicule INNER JOIN stage.utilise ON stage.vehicule.num = stage.utilise.vehicule WHERE utilise.sortie='".$_SESSION['pdfnum']."';");
$v = 0;
while ($liste_pdf4 = $pdf5->fetch()){
	$_SESSION['pdfvehiculeimmatriculation'][$v] = $liste_pdf4['immatriculation'];
	$_SESSION['pdfvehiculemodele'][$v] = $liste_pdf4['modele'];
	$v++;
}
$_SESSION['pdfvehicule'] = $v;

//requête permettant de récupérer tous les libelles de la table "classe" en fonction du "num" de sortie

$pdf6 = $conn->query("SELECT libelle FROM stage.classe INNER JOIN stage.concerne ON stage.classe.num = stage.concerne.classe WHERE concerne.sortie='".$_SESSION['pdfnum']."';");
$c = 0;
while ($liste_pdf5 = $pdf6->fetch()){
	$_SESSION['pdfclasselibelle'][$c] = $liste_pdf5['libelle'];
	$c++;
}
$_SESSION['pdfclasse'] = $c;

//requête permettant de récupérer tous les prenoms et noms des utilisateurs de la table "accompagne" en fonction du "num" de sortie

$pdf7 = $conn->query("SELECT prenom, nom FROM stage.utilisateur INNER JOIN stage.accompagne ON stage.utilisateur.id = stage.accompagne.utilisateur WHERE accompagne.sortie='".$_SESSION['pdfnum']."';");
$u = 0;
while ($liste_pdf6 = $pdf7->fetch()){
	$_SESSION['pdfutilisateurprenom'][$u] = $liste_pdf6['prenom'];
	$_SESSION['pdfutilisateurnom'][$u] = $liste_pdf6['nom'];
	$u++;
}
$_SESSION['pdfutilisateur'] = $u;

//requête permettant de récupérer tous les libelles, cours et propositions de la table "remplacement" en fonction du "num" de sortie

$pdf8 = $conn->query("SELECT libelle, cours, proposition FROM stage.remplacement INNER JOIN stage.classe ON stage.remplacement.classe = stage.classe.num WHERE remplacement.sortie='".$_SESSION['pdfnum']."';");
$r = 0;
while ($liste_pdf7 = $pdf8->fetch()){
	$_SESSION['pdfremplacementlibelle'][$r] = $liste_pdf7['libelle'];
	$_SESSION['pdfremplacementcours'][$r] = $liste_pdf7['cours'];
	$_SESSION['pdfremplacementproposition'][$r] = $liste_pdf7['proposition'];
	$r++;
}
$_SESSION['pdfremplacement'] = $r;

//requête permettant de récupérer tous les libelles, cours et propositions de la table "remplacement" en fonction du "num" de sortie

$pdf9 = $conn->query("SELECT libelle, classe, nombre FROM stage.invite INNER JOIN stage.centre ON stage.invite.centre = stage.centre.num WHERE invite.sortie='".$_SESSION['pdfnum']."';");
$g = 0;
while ($liste_pdf8 = $pdf9->fetch()){
	$_SESSION['pdfinvitecentre'][$g] = $liste_pdf8['libelle'];
	$_SESSION['pdfinviteclasse'][$g] = $liste_pdf8['classe'];
	$_SESSION['pdfinvitenombre'][$g] = $liste_pdf8['nombre'];
	$g++;
}
$_SESSION['pdfinvite'] = $g;

//permet de se diriger vers le fichier "pdf.php"

header("Location: pdf.php");
?>
