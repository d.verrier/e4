<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//permet de vérifier que l'utilisateur connecté à pour statut "enseignant" ou "non-enseignant"

if ($_SESSION['statut']==1 || $_SESSION['statut']==6) {
	if(isset($_SESSION['demande_Vea']) && $_SESSION['demande_Vea'] == 1) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Formulaire de demande :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<br>
							<h3 style="color: red;" align="center">Attention : </h3>
<?php
							if(isset($_SESSION['verification1']) && $_SESSION['verification1'] == 1) {
?>
								<br>
								<h5 style="color: red;">La date de sortie est inférieure ou égale à la date d'aujourd'hui ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification2']) && $_SESSION['verification2'] == 1) {
?>
								<br>
								<h5 style="color: red;">L'heure de retour doit être supérieur à l'heure de départ ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification3']) && $_SESSION['verification3'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez cocher les classes participant à la sortie ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification4']) && $_SESSION['verification4'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez cocher les accompagnateurs participant à la sortie ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification5']) && $_SESSION['verification5'] == 1) {
?>
								<br>
								<h5 style="color: red;">Nombre total d'apprenants = Interne(s) + Demi-Pensionnaire(s) + Externe(s) ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification6']) && $_SESSION['verification6'] == 1) {
?>
								<br>
								<h5 style="color: red;">Veuillez indiquer une heure d'enlèvement en cuisine ! </h5>
<?php
							}
?>
<?php
							if(isset($_SESSION['verification7']) && $_SESSION['verification7'] == 1) {
?>
								<br>
								<h5 style="color: red;">Le nombre de repas végétariens est superieur au nombre total de repas ! </h5>
<?php
							}
?>
							<br>
							<hr style="border-color: green">
							<p align="center"><b>Résidence administrative :</b> EPLEFPA La Germinière 72700 ROUILLON&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							<b>Centre : </b>LEGTA</p>
							<hr style="border-color: green">
							<label for="nature"><b>Nature de la mission :</b></label>
							<input class="form-control" id="nature" name="nature" placeholder="Objectif..." type="text" value="<?php echo $_SESSION['natureVe'] ?>"/>
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="dateSortie"><b>Date de sortie :</b></label>
									<input class="form-control" id="dateSortie" name="dateSortie" type="date" value="<?php echo $_SESSION['dateSortieVe'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureDepart"><b>Heure de départ : </b></label>
									<input class="form-control" id="heureDepart" name="heureDepart" type="time" value="<?php echo $_SESSION['heureDepartVe'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureRetour"><b>Heure de retour : </b></label>
									<input class="form-control" id="heureRetour" name="heureRetour" type="time" value="<?php echo $_SESSION['heureRetourVe'] ?>" required />
								</div>
							</div>
							<label for="destination"><b>Destination :</b></label>
							<input class="form-control" id="destination" name="destination" type="text" value="<?php echo $_SESSION['destinationVe'] ?>" required />
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="NbKm"><b>Nombre de kilomètres A/R : </b></label>
									<input class="form-control" id="NbKm" name="NbKm" min="0" type="number" value="<?php echo $_SESSION['NbKmVe'] ?>" required />
								</div>
							</div>
							<hr style="border-color: green">
							<label for="lienReferentiel"><b>Lien avec le référentiel :</b></label>
							<input class="form-control" id="lienReferentiel" name="lienReferentiel" type="text" value="<?php echo $_SESSION['lienReferentielVe'] ?>" />
							<br>
							<b><a target="_blank" href="http://s509545027.onlinehome.fr/grrminiere/login.php">Véhicule(s) emprunté(s) (consulter GRR pour les disponibilités)</a></b>
							<br>
							<br>
	<?php
							$tabT5 = array();
							foreach ($_SESSION['vehiculesVe'] as $utiliseVVe) {
								$tabT5[$utiliseVVe]="checked";
							}
								
							//requête de sélection de l'immatriculation et du modèle de la table "véhicule"

							$utilise = $conn->query("SELECT num, immatriculation, modele from stage.vehicule;");
							$i=0;
							while ($liste_utilise = $utilise->fetch()){
								if (isset($tabT5[$liste_utilise['num']])) {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="vehicules[]" id="<?php echo 'vehicules' . $i; ?>" value="<?php echo $liste_utilise['num']; ?>" <?php echo "checked"; ?>>
								<label for="<?php echo 'vehicules' . $i; ?>"><i>
	<?php
										echo $liste_utilise['immatriculation'];
										echo ' - <b>' . $liste_utilise['modele'];
	?>
										</b></i></label>
	<?php
								}
								else {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="vehicules[]" id="<?php echo 'vehicules' . $i; ?>" value="<?php echo $liste_utilise['num']; ?>">
								<label for="<?php echo 'vehicules' . $i; ?>"><i>
	<?php
										echo $liste_utilise['immatriculation'];
										echo ' - <b>' . $liste_utilise['modele'];
	?>
										</b></i></label>
	<?php
								}
								$i = $i + 1;
								if ($i % 4 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							&nbsp; &nbsp; &nbsp;
							<br>
							<label for="nbTicketSetram"><b>Bus SETRAM :</b></label><br><i><label for="nbTicketSetram"><b>Nombre de tickets à prévoir :</b></label></i><input class="form-control" id="nbTicketSetram" name="nbTicketSetram" min="0" type="number" value="<?php echo $_SESSION['nbTicketSetramVe'] ?>" />
							<br>
							<hr style="border-color: green">
							<b>Classe(s) ou groupe(s) :</b>
							<br>
							<br>
	<?php
							$tabT6 = array();
							foreach ($_SESSION['classesVe'] as $concerneCVe) {
								$tabT6[$concerneCVe]="checked";
							}
							
							//requête de sélection du num et du libelle de la table "classe"

							$concerne = $conn->query("SELECT num, libelle from stage.classe;");
							$i=0;
							while ($liste_concerne = $concerne->fetch()){
								if (isset($tabT6[$liste_concerne['num']])) {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="classes[]" id="<?php echo 'classes' . $i; ?>" value="<?php echo $liste_concerne['num']; ?>" <?php echo "checked"; ?>>
								<label for="<?php echo 'classes' . $i; ?>"><i>
	<?php
										echo $liste_concerne['libelle'];
	?>
										</b></i></label>
	<?php
								}
								else {
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="classes[]" id="<?php echo 'classes' . $i; ?>" value="<?php echo $liste_concerne['num']; ?>">
								<label for="<?php echo 'classes' . $i; ?>"><i>
	<?php
										echo $liste_concerne['libelle'];
	?>
										</b></i></label>
	<?php
								}
								$i = $i + 1;
								if ($i % 5 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="10" style="background-color: green; color: white" value="<?php echo "Autre centre" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre d'apprenants" ?> "><br><br>
								<select name="centrei1" id="centrei1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT2 = array();
							$tabT2[$_SESSION['centrei1Ve']]="selected";

							//requête de sélection du num et du libelle de la table "centre"

							$centrei1 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei1 = $centrei1->fetch()){
								if (isset($tabT2[$liste_centrei1['num']])) {
	?>
									<option value=<?php echo "".$liste_centrei1['num']?> <?php echo "selected"; ?>> <?php echo $liste_centrei1['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_centrei1['num']?>> <?php echo $liste_centrei1['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei1" id="Classei1" value="<?php echo $_SESSION['Classei1Ve'] ?>"> &nbsp;&nbsp; <input type="number" min="0" name="nbi1" id="nbi1" value="<?php echo $_SESSION['nbi1Ve'] ?>"><br><br>
								<select name="centrei2" id="centrei2" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT3 = array();
							$tabT3[$_SESSION['centrei2Ve']]="selected";

							//requête de sélection du num et du libelle de la table "centre"

							$centrei2 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei2 = $centrei2->fetch()){
								if (isset($tabT3[$liste_centrei2['num']])) {
	?>
									<option value=<?php echo "".$liste_centrei2['num']?> <?php echo "selected"; ?>> <?php echo $liste_centrei2['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_centrei2['num']?>> <?php echo $liste_centrei2['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei2" id="Classei2" value="<?php echo $_SESSION['Classei2Ve'] ?>"> &nbsp;&nbsp; <input type="number" min="0" name="nbi2" id="nbi2" value="<?php echo $_SESSION['nbi2Ve'] ?>"><br><br>
							</div>
							<br>
							<hr>
							<label for="nbEleves"><b>Nombre total d'apprenants :</b></label>
							<input class="form-control" id="nbEleves" name="nbEleves" type="number" min="0" value="<?php echo $_SESSION['nbElevesVe'] ?>" required />
							<br>
							<p align="center"> <b>Responsable :</b>
							<br>
							<br>
							<select id="responsable" name="responsable" required>
	<?php
							$tabT1 = array();
							$tabT1[$_SESSION['responsableVe']]="selected";						
							
							//requête de sélection de l'id, du prénom et du nom de la table "utilisateur"
							
							$responsable = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
							while($liste_responsable = $responsable->fetch()){
								if (isset($tabT1[$liste_responsable['id']])) {
	?>
									<option value=<?php echo "".$liste_responsable['id']?> <?php echo "selected"; ?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
								else {
	?>	
									<option value=<?php echo "".$liste_responsable['id']?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
							}
	?>
							</select>
							<hr style="border-color: green">
							<b>Accompagnateur(s) :</b>
							<br>
							<br>
							<select multiple name="utilisateurs[]" id="utilisateurs[]" size="5" >
	<?php
								$tabT4 = array();
								foreach ($_SESSION['utilisateursVe'] as $accompagneUVe) {
									$tabT4[$accompagneUVe]="selected";
								}

								//requête de sélection de l'id, du prénom et du nom de la table "utilisateur" rangé par nom "croissant"

								$accompagne = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
								while ($liste_accompagne = $accompagne->fetch()){
									if (isset($tabT4[$liste_accompagne['id']])) {
	?>
										<option value=<?php echo "".$liste_accompagne['id']?> <?php echo "selected"; ?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
									else {
	?>
										<option value=<?php echo "".$liste_accompagne['id']?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
								}
	?>
							</select>
							<br>
							(Ctrl+Clic)
							<hr style="border-color: green">
							<p align="center"> <b>Remplacement(s) :</b>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Cours concerné" ?> "> &nbsp;&nbsp; <input disabled type="text" style="background-color: green; color: white" value="<?php echo "Remplacement proposé" ?> "><br><br>
								<select name="classe1" id="classe1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT7 = array();
							$tabT7[$_SESSION['classe1Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer1 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe1 = $classer1->fetch()){
								if (isset($tabT7[$liste_classe1['num']])) {
	?>
									<option value=<?php echo "".$liste_classe1['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe1['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe1['num']?>> <?php echo $liste_classe1['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours1" id="Cours1" value="<?php echo $_SESSION['Cours1Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition1" id="proposition1" value="<?php echo $_SESSION['proposition1Ve'] ?>"><br><br>
								<select name="classe2" id="classe2">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT8 = array();
							$tabT8[$_SESSION['classe2Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer2 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe2 = $classer2->fetch()){
								if (isset($tabT8[$liste_classe2['num']])) {
	?>
									<option value=<?php echo "".$liste_classe2['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe2['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe2['num']?>> <?php echo $liste_classe2['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours2" id="Cours2" value="<?php echo $_SESSION['Cours2Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition2" id="proposition2" value="<?php echo $_SESSION['proposition2Ve'] ?>"><br><br>
								<select name="classe3" id="classe3">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT9 = array();
							$tabT9[$_SESSION['classe3Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer3 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe3 = $classer3->fetch()){
								if (isset($tabT9[$liste_classe3['num']])) {
	?>
									<option value=<?php echo "".$liste_classe3['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe3['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe3['num']?>> <?php echo $liste_classe3['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours3" id="Cours3" value="<?php echo $_SESSION['Cours3Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition3" id="proposition3" value="<?php echo $_SESSION['proposition3Ve'] ?>"><br><br>
								<select name="classe4" id="classe4">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT10 = array();
							$tabT10[$_SESSION['classe4Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer4 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe4 = $classer4->fetch()){
								if (isset($tabT10[$liste_classe4['num']])) {
	?>
									<option value=<?php echo "".$liste_classe4['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe4['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe4['num']?>> <?php echo $liste_classe4['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours4" id="Cours4" value="<?php echo $_SESSION['Cours4Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition4" id="proposition4" value="<?php echo $_SESSION['proposition4Ve'] ?>"><br><br>
								<select name="classe5" id="classe5">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT11 = array();
							$tabT11[$_SESSION['classe5Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer5 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe5 = $classer5->fetch()){
								if (isset($tabT11[$liste_classe5['num']])) {
	?>
									<option value=<?php echo "".$liste_classe5['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe5['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe5['num']?>> <?php echo $liste_classe5['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours5" id="Cours5" value="<?php echo $_SESSION['Cours5Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition5" id="proposition5" value="<?php echo $_SESSION['proposition5Ve'] ?>"><br><br>
								<select name="classe6" id="classe6">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT12 = array();
							$tabT12[$_SESSION['classe6Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer6 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe6 = $classer6->fetch()){
								if (isset($tabT12[$liste_classe6['num']])) {
	?>
									<option value=<?php echo "".$liste_classe6['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe6['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe6['num']?>> <?php echo $liste_classe6['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours6" id="Cours6" value="<?php echo $_SESSION['Cours6Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition6" id="proposition6" value="<?php echo $_SESSION['proposition6Ve'] ?>"><br><br>
								<select name="classe7" id="classe7">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT13 = array();
							$tabT13[$_SESSION['classe7Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer7 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe7 = $classer7->fetch()){
								if (isset($tabT13[$liste_classe7['num']])) {
	?>
									<option value=<?php echo "".$liste_classe7['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe7['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe7['num']?>> <?php echo $liste_classe7['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours7" id="Cours7" value="<?php echo $_SESSION['Cours7Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition7" id="proposition7" value="<?php echo $_SESSION['proposition7Ve'] ?>"><br><br>
								<select name="classe8" id="classe8">
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							$tabT14 = array();
							$tabT14[$_SESSION['classe8Ve']]="selected";

							//requête de sélection du num et du libelle de la table "classe"

							$classer8 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe8 = $classer8->fetch()){
								if (isset($tabT14[$liste_classe8['num']])) {
	?>
									<option value=<?php echo "".$liste_classe8['num']?> <?php echo "selected"; ?>> <?php echo $liste_classe8['libelle'] ;?></option>
	<?php
								}
								else {
	?>
									<option value=<?php echo "".$liste_classe8['num']?>> <?php echo $liste_classe8['libelle'] ;?></option>
	<?php
								}
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours8" id="Cours8" value="<?php echo $_SESSION['Cours8Ve'] ?>"> &nbsp;&nbsp; <input type="text" name="proposition8" id="proposition8" value="<?php echo $_SESSION['proposition8Ve'] ?>"><br><br>
							</div>
							<hr style="border-color: green">
							<b>Choix du repas :</b>
							<br>
							&nbsp; &nbsp; &nbsp;
							<div class="row">
								&nbsp; &nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasNon" value="Pas de Repas" <?php if( $_SESSION['RepasVe'] == "Pas de Repas"){  echo "checked"; }?> >
								<label for="RepasNon"><i>Pas de repas</i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasSecs" value="Repas Secs" <?php if( $_SESSION['RepasVe'] == "Repas Secs"){  echo "checked"; }?> >
								<label for="RepasSecs"><i>Repas Secs (</i>à utiliser pour déplacements, <i><b>pas de conservation au froid)</b></i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasRefrigeres" value="Repas Réfrigérés" <?php if( $_SESSION['RepasVe'] == "Repas Réfrigérés"){  echo "checked"; }?> >
								<label for="RepasRefrigeres"><i>Repas réfrigérés (</i>repas complet <i><b>tenus au frais en glacière)</b></i></label>
							</div>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Repas Végétariens" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre de repas " ?> "><br><br>
								<select name="vegetarien" id="vegetarien" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php
							if ($_SESSION['vegetarienVe'] == "Oui") {
	?>
								<option value="Oui" selected >Oui</option>
								<option value="Non">Non</option>
	<?php
							}
							else if ($_SESSION['vegetarienVe'] == "Non") {
	?>
								<option value="Oui">Oui</option>
								<option value="Non" selected >Non</option>
	<?php
							}
							else {
	?>
								<option value="Oui">Oui</option>
								<option value="Non">Non</option>
	<?php
							}
	?>
								</select>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" min="0" name="nbVegetarien" id="nbVegetarien" value="<?php echo $_SESSION['nbVegetarienVe'] ?>"><br><br>
							</div>
							<br>
							<br>
							<b>Nombre total de repas à réserver : </b>
							<br>
							<hr>
							<br>
							<b>Apprenants :</b>
							<br>
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="nbInterne"><b>Interne(s) : </b></label>
									<input class="form-control" id="nbInterne" name="nbInterne" type="number" min="0" value="<?php echo $_SESSION['nbInterneVe'] ?>" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbDP"><b>Demi-Pensionnaire(s) : </b></label>
									<input class="form-control" id="nbDP" name="nbDP" type="number" min="0" value="<?php echo $_SESSION['nbDPVe'] ?>" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbExterne"><b>Externe(s) : </b></label>
									<input class="form-control" id="nbExterne" name="nbExterne" type="number" min="0" value="<?php echo $_SESSION['nbExterneVe'] ?>" />
								</div>
							</div>
							<hr>
							<label for="heureEnlevement"><b>Heure d'enlèvement en cuisine : </b>
							<input class="form-control" id="heureEnlevement" name="heureEnlevement" type="time" value="<?php echo $_SESSION['heureEnlevementVe'] ?>" /></label>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
	else {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Formulaire de demande :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<p align="center"><b>Résidence administrative :</b> EPLEFPA La Germinière 72700 ROUILLON&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							<b>Centre : </b>LEGTA</p>
							<hr style="border-color: green">
							<label for="nature"><b>Nature de la mission :</b></label>
							<input class="form-control" id="nature" name="nature" placeholder="Objectif..." type="text" />
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="dateSortie"><b>Date de sortie :</b></label>
									<input class="form-control" id="dateSortie" name="dateSortie" type="date" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureDepart"><b>Heure de départ : </b></label>
									<input class="form-control" id="heureDepart" name="heureDepart" type="time" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="heureRetour"><b>Heure de retour : </b></label>
									<input class="form-control" id="heureRetour" name="heureRetour" type="time" required />
								</div>
							</div>
							<label for="destination"><b>Destination :</b></label>
							<input class="form-control" id="destination" name="destination" type="text" required />
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="NbKm"><b>Nombre de kilomètres A/R : </b></label>
									<input class="form-control" id="NbKm" name="NbKm" min="0" type="number" required />
								</div>
							</div>
							<hr style="border-color: green">
							<label for="lienReferentiel"><b>Lien avec le référentiel :</b></label>
							<input class="form-control" id="lienReferentiel" name="lienReferentiel" type="text" />
							<br>
							<b><a target="_blank" href="http://s509545027.onlinehome.fr/grrminiere/login.php">Véhicule(s) emprunté(s) (consulter GRR pour les disponibilités)</a></b>
							<br>
							<br>
	<?php

							//requête de sélection de l'immatriculation et du modèle de la table "véhicule"

							$utilise = $conn->query("SELECT num, immatriculation, modele from stage.vehicule;");
							$i=0;
							while ($liste_utilise = $utilise->fetch()){
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="vehicules[]" id="<?php echo 'vehicules' . $i; ?>" value="<?php echo $liste_utilise['num']; ?>">
								<label for="<?php echo 'vehicules' . $i; ?>"><i>
	<?php
										echo $liste_utilise['immatriculation'];
										echo ' - <b>' . $liste_utilise['modele'];
	?>
										</b></i></label>
	<?php
								$i = $i + 1;
								if ($i % 4 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							&nbsp; &nbsp; &nbsp;
							<br>
							<label for="nbTicketSetram"><b>Bus SETRAM :</b></label><br><i><label for="nbTicketSetram"><b>Nombre de tickets à prévoir :</b></label></i><input class="form-control" id="nbTicketSetram" name="nbTicketSetram" min="0" type="number"/>
							<br>
							<hr style="border-color: green">
							<b>Classe(s) ou groupe(s) :</b>
							<br>
							<br>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$concerne = $conn->query("SELECT num, libelle from stage.classe;");
							$i=0;
							while ($liste_concerne = $concerne->fetch()){
	?>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<input type="checkbox" name="classes[]" id="<?php echo 'classes' . $i; ?>" value="<?php echo $liste_concerne['num']; ?>">
								<label for="<?php echo 'classes' . $i; ?>"><i><b>
	<?php
										echo $liste_concerne['libelle'];
	?>
										</b></i></label>
	<?php
								$i = $i + 1;
								if ($i % 5 == 0){
	?>
									<br>
	<?php
								}
							}
	?>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="10" style="background-color: green; color: white" value="<?php echo "Autre centre" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre d'apprenants" ?> "><br><br>
								<select name="centrei1" id="centrei1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "centre"

							$centrei1 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei1 = $centrei1->fetch()){
	?>
								<option value=<?php echo "".$liste_centrei1['num']?>> <?php echo $liste_centrei1['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei1" id="Classei1"> &nbsp;&nbsp; <input type="number" min="0" name="nbi1" id="nbi1" ><br><br>
								<select name="centrei2" id="centrei2" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "centre"

							$centrei2 = $conn->query("SELECT num, libelle FROM stage.centre WHERE num != 1;");
							while($liste_centrei2 = $centrei2->fetch()){
	?>
								<option value=<?php echo "".$liste_centrei2['num']?>> <?php echo $liste_centrei2['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Classei2" id="Classei2"> &nbsp;&nbsp; <input type="number" min="0" name="nbi2" id="nbi2" ><br><br>
							</div>
							<br>
							<hr>
							<label for="nbEleves"><b>Nombre total d'apprenants :</b></label>
							<input class="form-control" id="nbEleves" name="nbEleves" type="number" min="0" required />
							<br>
							<p align="center"> <b>Responsable :</b>
							<br>
							<br>
							<select id="responsable" name="responsable" required>
	<?php

							//requête de sélection de l'id de la table "utilisateur" de l'utilisateur connecté

							$srequeteTest1 = "SELECT id from stage.utilisateur where id = ".$_SESSION['id'].";";
							$liste_Test1 = $conn->query($srequeteTest1);
							$tabT1 = array();
							
							while ($id_test1 = $liste_Test1->fetch()){
								$tabT1[$id_test1['id']]="selected";
							}						
							
							//requête de sélection de l'id, du prénom et du nom de la table "utilisateur" rangé par nom "croissant"
							
							$responsable = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
							while($liste_responsable = $responsable->fetch()){
								if (isset($tabT1[$liste_responsable['id']])) {
	?>
									<option value=<?php echo "".$liste_responsable['id']?> <?php echo "selected"; ?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
								else {
	?>	
									<option value=<?php echo "".$liste_responsable['id']?>> <?php echo $liste_responsable['prenom']," ",$liste_responsable['nom'] ;?></option>
	<?php
								}
							}
	?>
							</select>
							<hr style="border-color: green">
							<b>Accompagnateur(s) :</b>
							<br>
							<br>
							<select multiple name="utilisateurs[]" id="utilisateurs[]" size="5" >
	<?php
								//requête de sélection de l'id de la table "utilisateur" de l'utilisateur connecté

								$ssrequeteTest1 = "SELECT id from stage.utilisateur where id = ".$_SESSION['id'].";";
								$sliste_Test1 = $conn->query($ssrequeteTest1);
								$stabT1 = array();
							
								while ($sid_test1 = $sliste_Test1->fetch()){
									$stabT1[$sid_test1['id']]="selected";
								}

								//requête de sélection de l'id, du prénom et du nom de la table "utilisateur" rangé par nom "croissant"

								$accompagne = $conn->query("SELECT id, prenom, nom FROM stage.utilisateur ORDER BY nom ASC;");
								while ($liste_accompagne = $accompagne->fetch()){
									if (isset($stabT1[$liste_accompagne['id']])) {
	?>
										<option value=<?php echo "".$liste_accompagne['id']?> <?php echo "selected"; ?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
									else {
	?>	
										<option value=<?php echo "".$liste_accompagne['id']?>> <?php echo $liste_accompagne['prenom']," ",$liste_accompagne['nom'] ;?></option>
	<?php
									}
								}
	?>
							</select>
							<br>
							(Ctrl+Clic)
							<hr style="border-color: green">
							<p align="center"> <b>Remplacement(s) :</b>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Classe concernée" ?> "> &nbsp;&nbsp; <input disabled type="text"style="background-color: green; color: white" value="<?php echo "Cours concerné" ?> "> &nbsp;&nbsp; <input disabled type="text" style="background-color: green; color: white" value="<?php echo "Remplacement proposé" ?> "><br><br>
								<select name="classe1" id="classe1" >
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer1 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe1 = $classer1->fetch()){
	?>
								<option value=<?php echo "".$liste_classe1['num']?>> <?php echo $liste_classe1['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours1" id="Cours1"> &nbsp;&nbsp; <input type="text" name="proposition1" id="proposition1"><br><br>
								<select name="classe2" id="classe2">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer2 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe2 = $classer2->fetch()){
	?>
								<option value=<?php echo "".$liste_classe2['num']?>> <?php echo $liste_classe2['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours2" id="Cours2"> &nbsp;&nbsp; <input type="text" name="proposition2" id="proposition2"><br><br>
								<select name="classe3" id="classe3">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer3 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe3 = $classer3->fetch()){
	?>
								<option value=<?php echo "".$liste_classe3['num']?>> <?php echo $liste_classe3['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours3" id="Cours3"> &nbsp;&nbsp; <input type="text" name="proposition3" id="proposition3"><br><br>
								<select name="classe4" id="classe4">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer4 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe4 = $classer4->fetch()){
	?>
								<option value=<?php echo "".$liste_classe4['num']?>> <?php echo $liste_classe4['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours4" id="Cours4"> &nbsp;&nbsp; <input type="text" name="proposition4" id="proposition4"><br><br>
								<select name="classe5" id="classe5">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer5 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe5 = $classer5->fetch()){
	?>
								<option value=<?php echo "".$liste_classe5['num']?>> <?php echo $liste_classe5['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours5" id="Cours5"> &nbsp;&nbsp; <input type="text" name="proposition5" id="proposition5"><br><br>
								<select name="classe6" id="classe6">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer6 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe6 = $classer6->fetch()){
	?>
								<option value=<?php echo "".$liste_classe6['num']?>> <?php echo $liste_classe6['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours6" id="Cours6"> &nbsp;&nbsp; <input type="text" name="proposition6" id="proposition6"><br><br>
								<select name="classe7" id="classe7">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer7 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe7 = $classer7->fetch()){
	?>
								<option value=<?php echo "".$liste_classe7['num']?>> <?php echo $liste_classe7['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours7" id="Cours7"> &nbsp;&nbsp; <input type="text" name="proposition7" id="proposition7"><br><br>
								<select name="classe8" id="classe8">
								<option selected disabled hidden>Choisissez ici</option>
	<?php

							//requête de sélection du num et du libelle de la table "classe"

							$classer8 = $conn->query("SELECT num, libelle FROM stage.classe;");
							while($liste_classe8 = $classer8->fetch()){
	?>
								<option value=<?php echo "".$liste_classe8['num']?>> <?php echo $liste_classe8['libelle'] ;?></option>
	<?php
							}
	?>
								</select> &nbsp;&nbsp; <input type="text" name="Cours8" id="Cours8"> &nbsp;&nbsp; <input type="text" name="proposition8" id="proposition8"><br><br>
							</div>
							<hr style="border-color: green">
							<b>Choix du repas :</b>
							<br>
							&nbsp; &nbsp; &nbsp;
							<div class="row">
								&nbsp; &nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasNon" value="Pas de Repas" checked>
								<label for="RepasNon"><i>Pas de repas</i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasSecs" value="Repas Secs" >
								<label for="RepasSecs"><i>Repas Secs (</i>à utiliser pour déplacements, <i><b>pas de conservation au froid)</b></i></label>
								&nbsp; &nbsp; &nbsp;
								<input type="radio" name="Repas" id="RepasRefrigeres" value="Repas Réfrigérés" >
								<label for="RepasRefrigeres"><i>Repas réfrigérés (</i>repas complet <i><b>tenus au frais en glacière)</b></i></label>
							</div>
							<br>
							<hr>
							<div class="table-responsive bg-light">
								<br>
								<input disabled type="text" size="12" style="background-color: green; color: white" value="<?php echo "Repas Végétariens" ?> "> &nbsp;&nbsp; <input disabled type="text" size="16" style="background-color: green; color: white" value="<?php echo "Nombre de repas " ?> "><br><br>
								<select name="vegetarien" id="vegetarien" >
								<option selected disabled hidden>Choisissez ici</option>
								<option value="Oui">Oui</option>
								<option value="Non">Non</option>
								</select>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" min="0" name="nbVegetarien" id="nbVegetarien"><br><br>
							</div>
							<br>
							<br>
							<b>Nombre total de repas à réserver : </b>
							<br>
							<hr>
							<br>
							<b>Apprenants :</b>
							<br>
							<br>
							<div class="row">
								<div class="form-group col text-center">
									<label for="nbInterne"><b>Interne(s) : </b></label>
									<input class="form-control" id="nbInterne" name="nbInterne" type="number" min="0" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbDP"><b>Demi-Pensionnaire(s) : </b></label>
									<input class="form-control" id="nbDP" name="nbDP" type="number" min="0" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="nbExterne"><b>Externe(s) : </b></label>
									<input class="form-control" id="nbExterne" name="nbExterne" type="number" min="0" />
								</div>
							</div>
							<hr>
							<label for="heureEnlevement"><b>Heure d'enlèvement en cuisine : </b>
							<input class="form-control" id="heureEnlevement" name="heureEnlevement" type="time" /></label>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>
