<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//permet de vérifier que l'utilisateur connecté à pour statut "enseignant" ou "non-enseignant"

if ($_SESSION['statut']==2 || $_SESSION['statut']==3 || $_SESSION['statut']==4 || $_SESSION['statut']==5) {
?>
	<div class="container-fluid" align="center">
		<br>
		<br>
		<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
			<br>
			<h1 style="font-family: 'Gentium Book Basic'">Choisissez la période de sortie à télécharger :</h1>
			<br>
		</div>
		<div class="tab-content">
			<br>
			<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
				<form class="form-horizontal" method="post" action = "telecharger.php">
					<div class="alert alert-secondary">
						<p>
						<div class="row">
							<div class="form-group col text-center">
								<label for="dateSortie"><b>Date de debut :</b></label>
								<input class="form-control" id="dateDebut" name="dateDebut" type="date" required />
							</div>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							<div class="form-group col text-center">
								<label for="dateSortie"><b>Date de fin :</b></label>
								<input class="form-control" id="dateFin" name="dateFin" type="date" required />
							</div>
						</div>
						<hr style="border-color: green">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d afficher le message d erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	}
</style>
