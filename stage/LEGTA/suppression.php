<?php
session_start();

//permet de se connecter à la base de données MySQL

$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//récupération du "num" de sortie sélectionné dans la page de suppression de demandes

$num=$_POST['suppression'];

//envoie de mails automatiques aux utilisateurs dont leur statut à fait une "action"

$annulation = $conn->query("SELECT * FROM stage.sortie where num = ".$num.";");
$statut = $annulation->fetch();

$mailH = $conn->query("SELECT prenom, nom, mail, dateSortie, heureDepart, heureRetour FROM stage.utilisateur INNER JOIN stage.sortie ON stage.utilisateur.id = stage.sortie.connecte WHERE sortie.num = ".$num.";");
$donnees_mailH = $mailH->fetch();

$sujet = "Annulation d'une demande de sortie";
$message = "Bonjour, \n ".$donnees_mailH['prenom']." ".$donnees_mailH['nom']." vient d'annuler la demande de sortie du ".$donnees_mailH['dateSortie']." de ".$donnees_mailH['heureDepart']." à ".$donnees_mailH['heureRetour'].". \n\n Si vous souhaitez visualiser toutes les demandes de sortie, veuillez cliquer sur le lien suivant :\n https://eap72.fr/resa/LEGTA/tableau.php";
$headers = 'From: ' .$donnees_mailH['mail']."\r\n"."Content-Type: text/html; charset=utf-8 ";

if(isset($statut['economat'])){
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 4;");

	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
}

if(isset($statut['vieScolaire'])){
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 3;");

	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
}

if(isset($statut['proviseur'])){
	$mailD = $conn->query("SELECT mail FROM stage.utilisateur WHERE statut = 2;");

	while ($donnees_mailD = $mailD->fetch()){
		$destinataire = $donnees_mailD['mail'];

		mail($destinataire, $sujet, $message, $headers);
	}
}

//requête de suppression de ce qui est en rapport avec le "num" de la sortie sélectionné précédemment

$requete1 = ("DELETE FROM stage.accompagne WHERE sortie=".$num.";");
$conn ->exec($requete1);

$requete2 = ("DELETE FROM stage.concerne WHERE sortie=".$num.";");
$conn ->exec($requete2);

$requete3 = ("DELETE FROM stage.organise WHERE sortie=".$num.";");
$conn ->exec($requete3);

$requete4 = ("DELETE FROM stage.remplacement WHERE sortie=".$num.";");
$conn ->exec($requete4);

$requete5 = ("DELETE FROM stage.repas WHERE sortie=".$num.";");
$conn ->exec($requete5);

$requete6 = ("DELETE FROM stage.utilise WHERE sortie=".$num.";");
$conn ->exec($requete6);

$requete8 = ("DELETE FROM stage.invite WHERE sortie=".$num.";");
$conn ->exec($requete8);

$requete7 = ("DELETE FROM stage.sortie WHERE num=".$num.";");
$conn ->exec($requete7);

//permet de se diriger vers le fichier "tableau.php"

header("Location: tableau.php");
?>
