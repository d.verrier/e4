<?php
session_start();

//permet de supprimer les variables SESSION enregistrees tout au long de la connexion

unset($_SESSION['id']);
unset($_SESSION['prenom']);
unset($_SESSION['nom']);
unset($_SESSION['mail']);
unset($_SESSION['statut']);

//permet de se diriger vers la page "GestionDemande.php"

header("Location: ../GestionDemande.php");
?>
