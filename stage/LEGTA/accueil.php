<?php
session_start();
include 'entete.php';

//Page d'accueil pour les utilisateurs connectés

if (isset($_SESSION['statut'])){

	//Page d'accueil pour les utilisateurs ayant le statut enseignant ou non-enseignant

	if ($_SESSION['statut']==1 || $_SESSION['statut']==6) {
		unset($_SESSION['demande_modification']);
		unset($_SESSION['demande_Vea']);
		unset($_SESSION['demande_Vem']);
?>
		<div class="container-fluid">
			<br>
			<br>
			<hr>
			<h1 style="text-align: center"><b>LEGTA</b></h1>
			<hr>
			<div class="row bg-light">
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/tableau.php"><i class="fas fa-clipboard-list" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Tableau Récapitulatif</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/ajouter.php"><i class="fas fa-plus-circle" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Faire une demande</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/modifier.php"><i class="fas fa-edit" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Modifier une demande</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/annuler.php"><i class="fas fa-trash-alt" style="font-size: 100px"></i></a>
					<br>
					<hr>
					<p><b>Annuler une demande</b></p>
				</div>
			</div>
		</div>
		<hr>
<?php
	}

	//Page d'accueil pour les utilisateurs ayant le statut administrateur

	else if ($_SESSION['statut']==7) {
		unset($_SESSION['demande_modification']);
		unset($_SESSION['existea']);
		unset($_SESSION['existem']);
		unset($_SESSION['demande_modificationv']);
		unset($_SESSION['existeav']);
		unset($_SESSION['existemv']);
?>
		<div class="container-fluid">
			<br>
			<br>
			<hr>
			<h1 style="text-align: center"><b>LEGTA</b></h1>
			<hr>
			<div class="row bg-light">
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/compte.php"><i class="fas fa-user" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Gestion des comptes</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/vehicule.php"><i class="fas fa-car" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Gestion des vehicules</b></p>
				</div>
			</div>
		</div>
		<hr>
<?php
	}

	//Page d'accueil pour tous les utilisateurs ayant les autres statuts appartenant au LEGTA

	else {
?>
		<div class="container-fluid">
			<br>
			<br>
			<hr>
			<h1 style="text-align: center"><b>LEGTA</b></h1>
			<hr>
			<div class="row bg-light">
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/tableau.php"><i class="fas fa-clipboard-list" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Tableau Récapitulatif</b></p>
				</div>
				<div class="col-sm" align="center">
					<a href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/csv.php"><i class="fas fa-file-csv" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Tableau au format CSV</b></p>
				</div>
			</div>
		</div>
		<hr>
<?php
	}
}

//Page d'accueil pour les utilisateurs non-connectés

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connecté, merci de cliquer sur le bouton si dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" name="centre01" values="centre01" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	}
</style>
