<?php
session_start();
//connexion à la base de données MySQL
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

//vérification des variables(déclarer et contenant des valeurs non nulles)

if(isset($_POST['connexion'])) {

	//requête permettant de sélectionner l'identifiant et le mot de passe

	$requete1 = $conn->query("SELECT identifiant, mdp FROM stage.utilisateur WHERE identifiant='".$_POST['identifiant']."' AND mdp='".$_POST['mdp']."';");
	$connexion = $requete1->fetch();

	//vérification de la correspondance entre identifiants et mots de passe

    if ($connexion['identifiant']==$_POST['identifiant'] && $connexion['mdp']==$_POST['mdp'])
    {
		$requete2 = $conn->query("SELECT id, prenom, nom, mail, statut FROM stage.utilisateur WHERE identifiant='".$_POST['identifiant']."' AND mdp='".$_POST['mdp']."';");
		$session = $requete2->fetch();

		//variables SESSION permettant de garder en mémoire l'id, le mail et le statut de l'utilisateur connecté

		$_SESSION['id'] = $session['id'];
		$_SESSION['prenom'] = $session['prenom'];
		$_SESSION['nom'] = $session['nom'];
		$_SESSION['mail'] = $session['mail'];
		$_SESSION['statut'] = $session['statut'];

		//permet de se diriger vers la page "accueil.php"

        header('Location: accueil.php');
    }
    else {
?>
		<br>
		<!-- Message d'erreur (mot de passe et/ou identifiant incorrect) -->

        <div class="erreur">Identifiant ou mot de passe incorrect !</div>
<?php
    }
}
include 'entete.php';
?>

<br>
<div class="container-fluid" style="align-center">
    <form class="col text-center" method="post" action="connexion.php">
        <label for="identifiant"><b>Identifiant :</b></label>
        <input class="form-control" id="identifiant" name="identifiant" type="text" />
		<br>

        <label for="mdp"><b>Mot de passe :</b></label>
        <input class="form-control" id="mdp" name="mdp" type="password" />
		<br>
		<br>

        <button type="submit" name="connexion" values="connexion" class="btn btn-success">Connexion</button>
    </form>
</div>

<!-- Permet au message d'erreur d'être centré et coloré en rouge -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	}
</style>
