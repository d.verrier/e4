<?php
session_start();
include '../entete.php';
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");
if ($_SESSION['statut']==7) {
	if(isset($_SESSION['existea']) && $_SESSION['existea']==1) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Creation de compte :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<br>
							<h3 style="color: red;" align="center">Attention : Veuillez changer l'identifiant et/ou le mot de passe ! </h3>
							<br>
							<hr style="border-color: green">
							<p>
							<div class="row" >
								<div class="form-group col text-center">
									<label for="nom"><b>Nom :</b></label>
									<input class="form-control" id="nom" name="nom" type="text" value="<?php echo $_SESSION['nomEx'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="prenom"><b>Prenom :</b></label>
									<input class="form-control" id="prenom" name="prenom" type="text" value="<?php echo $_SESSION['prenomEx'] ?>" required />
								</div>
							</div>
							<div class="row">
								<div class="form-group col text-center">
									<label for="telephone"><b>Telephone :</b></label>
									<input class="form-control" id="telephone" name="telephone" type="tel" pattern="[0-9]{10}" value="<?php echo $_SESSION['telephoneEx'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="mail"><b>E-mail :</b></label>
									<input class="form-control" id="mail" name="mail" type="text" value="<?php echo $_SESSION['mailEx'] ?>" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="adresse"><b>Adresse :</b></label>
									<input class="form-control" id="adresse" name="adresse" type="text" value="<?php echo $_SESSION['adresseEx'] ?>" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="ville"><b>Ville :</b></label>
									<input class="form-control" id="ville" name="ville" type="text" value="<?php echo $_SESSION['villeEx'] ?>" />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="identifiant"><b>Identifiant :</b></label>
									<input class="form-control" id="identifiant" name="identifiant" type="text" value="<?php echo $_SESSION['identifiantEx'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="mdp"><b>Mot de passe :</b></label>
									<input class="form-control" id="mdp" name="mdp" type="password" value="<?php echo $_SESSION['mdpEx'] ?>" required />
								</div>
							</div>
							<p align="center"> <b>Statut :</b>
							<br>
							<br>
							<select name="statut" id="statut" required>
	<?php
								$tabT1 = array();
								$tabT1[$_SESSION['statutEx']]="selected";

								$statuer = $conn->query("SELECT num, libelle FROM stage.statut ORDER BY libelle;");
								while($liste_statut = $statuer->fetch()){
									if (isset($tabT1[$liste_statut['num']])) {
	?>
									<option value=<?php echo "".$liste_statut['num']?> <?php echo "selected"; ?>> <?php echo $liste_statut['libelle'] ;?></option>
	<?php
									}
									else {
	?>
									<option value=<?php echo "".$liste_statut['num']?>> <?php echo $liste_statut['libelle'] ;?></option>
	<?php
									}
								}
	?>
							</select>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
	else {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Creation de compte :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<p align="center">
							<div class="row" >
								<div class="form-group col text-center">
									<label for="nom"><b>Nom :</b></label>
									<input class="form-control" id="nom" name="nom" type="text" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="prenom"><b>Prenom :</b></label>
									<input class="form-control" id="prenom" name="prenom" type="text" required />
								</div>
							</div>
							<div class="row">
								<div class="form-group col text-center">
									<label for="telephone"><b>Telephone :</b></label>
									<input class="form-control" id="telephone" name="telephone" type="tel" pattern="[0-9]{10}" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="mail"><b>E-mail :</b></label>
									<input class="form-control" id="mail" name="mail" type="text" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="adresse"><b>Adresse :</b></label>
									<input class="form-control" id="adresse" name="adresse" type="text" />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="ville"><b>Ville :</b></label>
									<input class="form-control" id="ville" name="ville" type="text" />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="identifiant"><b>Identifiant :</b></label>
									<input class="form-control" id="identifiant" name="identifiant" type="text" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="mdp"><b>Mot de passe :</b></label>
									<input class="form-control" id="mdp" name="mdp" type="password" required />
								</div>
							</div>
							<p align="center"> <b>Statut :</b>
							<br>
							<br>
							<select name="statut" id="statut" required>
								<option selected disabled hidden>Choisissez ici</option>
	<?php
								$statuer = $conn->query("SELECT num, libelle FROM stage.statut ORDER BY libelle;");
								while($liste_statut = $statuer->fetch()){
	?>
									<option value=<?php echo "".$liste_statut['num']?>> <?php echo $liste_statut['libelle'] ;?></option>
	<?php
								}
	?>
							</select>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
}

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="../connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>
