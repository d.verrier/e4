<?php
session_start();
include '../entete.php';
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

if (isset($_SESSION['statut'])){

	if ($_SESSION['statut']==7) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif de tous les utilisateurs :</h1>
				<br>
			</div>
			<br>

			<style>
				table{
					width: 180%;
				}
			</style>

			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Telephone</th>
									<th>E-mail</th>
									<th>Statut</th>
								</tr>
							</thead>
							<tbody bgcolor="beige">
<?php
								$resultat = $conn->query("SELECT * FROM stage.utilisateur INNER JOIN stage.statut ON stage.utilisateur.statut = stage.statut.num;");
								while($donnee = $resultat->fetch()){
?>
									<tr value="<?php echo $donnee['id'] ?>" >
										<th scope="row"><?php echo $donnee['nom']; ?></th>
										<td><?php echo $donnee['prenom']; ?></td>
										<td><?php echo $donnee['telephone']; ?></td>
										<td><?php echo $donnee['mail']; ?></td>
										<td><?php echo $donnee['libelle']; ?></td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>
						<script>
							$(document).ready(function() {
								$('#tableau').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 0, "asc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
}

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>
