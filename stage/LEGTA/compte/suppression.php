<?php
session_start();
$conn = new PDO("pgsql:host=postgresql.bts-malraux72.net;port=5432;dbname=d.verrier;user=d.verrier;password=P@ssword");

$id=$_POST['suppression'];

$requete1 = ("DELETE FROM stage.accompagne WHERE utilisateur=".$id.";");
$conn ->exec($requete1);

$sortie1 = $conn->query("SELECT num FROM stage.sortie WHERE connecte = ".$id.";");

while ($liste_sortie1 = $sortie1->fetch()){
	$num1=$liste_sortie1['num'];

	$requete2 = ("DELETE FROM stage.accompagne WHERE sortie=".$num1.";");
	$conn ->exec($requete2);

	$requete3 = ("DELETE FROM stage.concerne WHERE sortie=".$num1.";");
	$conn ->exec($requete3);

	$requete4 = ("DELETE FROM stage.organise WHERE sortie=".$num1.";");
	$conn ->exec($requete4);

	$requete5 = ("DELETE FROM stage.remplacement WHERE sortie=".$num1.";");
	$conn ->exec($requete5);

	$requete6 = ("DELETE FROM stage.repas WHERE sortie=".$num1.";");
	$conn ->exec($requete6);

	$requete7 = ("DELETE FROM stage.utilise WHERE sortie=".$num1.";");
	$conn ->exec($requete7);

	$requete8 = ("DELETE FROM stage.sortie WHERE num=".$num1.";");
	$conn ->exec($requete8);
}

$sortie2 = $conn->query("SELECT num FROM stage.sortie WHERE responsable = ".$id.";");

while ($liste_sortie2 = $sortie2->fetch()){
	$num2=$liste_sortie2['num'];

	$requete9 = ("DELETE FROM stage.accompagne WHERE sortie=".$num2.";");
	$conn ->exec($requete9);

	$requete10 = ("DELETE FROM stage.concerne WHERE sortie=".$num2.";");
	$conn ->exec($requete10);

	$requete11 = ("DELETE FROM stage.organise WHERE sortie=".$num2.";");
	$conn ->exec($requete11);

	$requete12 = ("DELETE FROM stage.remplacement WHERE sortie=".$num2.";");
	$conn ->exec($requete12);

	$requete13 = ("DELETE FROM stage.repas WHERE sortie=".$num2.";");
	$conn ->exec($requete13);

	$requete14 = ("DELETE FROM stage.utilise WHERE sortie=".$num2.";");
	$conn ->exec($requete14);

	$requete15 = ("DELETE FROM stage.sortie WHERE num=".$num2.";");
	$conn ->exec($requete15);
}

$sortie3 = $conn->query("SELECT num FROM stage.sortie WHERE proviseur = ".$id.";");

while ($liste_sortie3 = $sortie3->fetch()){
	$num3=$liste_sortie3['num'];

	$requete16 = ("DELETE FROM stage.accompagne WHERE sortie=".$num3.";");
	$conn ->exec($requete16);

	$requete17 = ("DELETE FROM stage.concerne WHERE sortie=".$num3.";");
	$conn ->exec($requete17);

	$requete18 = ("DELETE FROM stage.organise WHERE sortie=".$num3.";");
	$conn ->exec($requete18);

	$requete19 = ("DELETE FROM stage.remplacement WHERE sortie=".$num3.";");
	$conn ->exec($requete19);

	$requete20 = ("DELETE FROM stage.repas WHERE sortie=".$num3.";");
	$conn ->exec($requete20);

	$requete21 = ("DELETE FROM stage.utilise WHERE sortie=".$num3.";");
	$conn ->exec($requete21);

	$requete22 = ("DELETE FROM stage.sortie WHERE num=".$num3.";");
	$conn ->exec($requete22);
}

$sortie4 = $conn->query("SELECT num FROM stage.sortie WHERE vieScolaire = ".$id.";");

while ($liste_sortie4 = $sortie4->fetch()){
	$num4=$liste_sortie4['num'];

	$requete23 = ("DELETE FROM stage.accompagne WHERE sortie=".$num4.";");
	$conn ->exec($requete23);

	$requete24 = ("DELETE FROM stage.concerne WHERE sortie=".$num4.";");
	$conn ->exec($requete24);

	$requete25 = ("DELETE FROM stage.organise WHERE sortie=".$num4.";");
	$conn ->exec($requete25);

	$requete26 = ("DELETE FROM stage.remplacement WHERE sortie=".$num4.";");
	$conn ->exec($requete26);

	$requete27 = ("DELETE FROM stage.repas WHERE sortie=".$num4.";");
	$conn ->exec($requete27);

	$requete28 = ("DELETE FROM stage.utilise WHERE sortie=".$num4.";");
	$conn ->exec($requete28);

	$requete29 = ("DELETE FROM stage.sortie WHERE num=".$num4.";");
	$conn ->exec($requete29);
}

$sortie5 = $conn->query("SELECT num FROM stage.sortie WHERE economat = ".$id.";");

while ($liste_sortie5 = $sortie5->fetch()){
	$num5=$liste_sortie5['num'];

	$requete30 = ("DELETE FROM stage.accompagne WHERE sortie=".$num5.";");
	$conn ->exec($requete30);

	$requete31 = ("DELETE FROM stage.concerne WHERE sortie=".$num5.";");
	$conn ->exec($requete31);

	$requete32 = ("DELETE FROM stage.organise WHERE sortie=".$num5.";");
	$conn ->exec($requete32);

	$requete33 = ("DELETE FROM stage.remplacement WHERE sortie=".$num5.";");
	$conn ->exec($requete33);

	$requete34 = ("DELETE FROM stage.repas WHERE sortie=".$num5.";");
	$conn ->exec($requete34);

	$requete35 = ("DELETE FROM stage.utilise WHERE sortie=".$num5.";");
	$conn ->exec($requete35);

	$requete36 = ("DELETE FROM stage.sortie WHERE num=".$num5.";");
	$conn ->exec($requete36);
}

$requete37 = ("DELETE FROM stage.utilisateur WHERE id=".$id.";");
$conn ->exec($requete37);

header("Location: tableau.php");
?>
