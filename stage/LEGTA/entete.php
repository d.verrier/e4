<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <meta charset="utf-8">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mr-auto">
			<!-- Affichage de l entête si l utilisateur est connecté -->
            <?php
            if (isset($_SESSION['statut'])){
            ?>
            <li class="nav-item active">
                <a class="nav-link" href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/accueil.php"><i class="fas fa-home"></i>&nbsp; &nbsp; Accueil</a>
            </li>
			<?php
            }
            ?>
		</ul>
		<ul class="navbar-nav mr-auto">
			<!-- Affichage de l entête si l utilisateur est connecté -->
			<?php
            if (isset($_SESSION['statut'])){
            ?>
			<li class="nav-item active">
                <a class="nav-link"><i class="fas fa-user"></i>&nbsp; &nbsp; <?php echo $_SESSION['prenom']. ' ' .strtoupper($_SESSION['nom']) ; ?></a>
            </li>
			<?php
            }
            ?>
		</ul>
		<ul class="navbar-nav justify-content-end">
			<!-- Affichage de l'entête si l'utilisateur est connecté -->
			<?php
            if (isset($_SESSION['statut'])){
            ?>
            <li class="nav-item active">
                <a class="nav-link" href="http://www.bts-malraux72.net/~d.verrier/e4/stage/LEGTA/deconnexion.php"><i class="fas fa-sign-out-alt"></i>&nbsp; &nbsp; Deconnexion</a>
            </li>
            <?php
            }
            ?>
        </ul>
    </div>
</nav>
<br>
<br>
</body>
</html>
