<?php
include 'LEGTA/entete.php';
?>
<br>
<div class="container-fluid" align="center">
	<div class="card bg-light mb-4"  style="max-width: 50%;">
		<br>
		<h1 align="center" style="font-family: 'Gentium Book Basic'"><b>Documentation Utilisateur</b></h1>
		<br>
	</div>
</div>
<div class="tab-content">
	<br>
	<div class="tab-pane fade active show">
		<div class="alert alert-secondary">
			<p>Cette application sert à gérer toutes les demandes de sortie.</p>
			<p>Lorsque vous êtes sur la page "http://eap72.fr/resa/GestionDemande.php", vous devez choisir le centre auquel vous appartenez en cliquant sur les boutons situés dans le cadre de chaque centre en dessous de chaque image de celui-ci.</p>
			<hr>
			<h1 align="center"><b>Sommaire</b></h1>
			<a href="#lycee"> 1 Lycee </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeadministrateur"> 1.1 Administrateur </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeenseignant"> 1.2 Enseignant et non enseignant </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeproviseuradjoint"> 1.3 Proviseur Adjoint </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeviescolaire"> 1.4 Vie Scolaire </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeeconomat"> 1.5 Economat </a><br>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="#lyceeinvite"> 1.6 Invite </a><br>
			<hr>
			<div id="lycee">
				<h1 align="center"><b>Lycée</b></h1>
				<br>
				<p>
				Lorsque vous avez cliqué sur le bouton "Demander" du lycée de la page "http://eap72.fr/resa/GestionDemande.php", une page d'authentification apparaît.
				<br>
				<br>
				- Si vous n'avez pas de compte, vous devez aller contacter un utilisateur ayant un statut d'administrateur dans cette application.
				<br>
				- Si vous avez un compte, vous devez renseigner votre identifiant et votre mot de passe dans les champs associés et cliquer sur le bouton "connexion".
				En cliquant sur ce bouton, vous arrivez sur une page d'accueil et une entête apparaît sur celle-ci.
				Cette entête est composé:
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- à gauche d'un bouton d'accueil permettant de se diriger vers la page d'accueil (celle où vous arrivez quand vous vous connectez)
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- au centre d'un affichage du prénom et du nom de celui qui est connecté
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- à droite d'un bouton de déconnexion permettant de se déconnecter et de se diriger vers la page "http://eap72.fr/resa/GestionDemande.php"
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeadministrateur">
				<h2 align="center"><b>Administrateur</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre la gestion de comptes et la gestion de vehicules en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Gestion de comptes</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de gestion de comptes, un menu apparaît et vous devez choisir entre un tableau récapitulatifs de tous les utilisateurs, l'ajout d'un compte, la modification d'un compte et la suppression d'un compte en cliquant sur les icones appropriés.
				<br>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatifs, un tableau de tous les utilisateurs apparaît avec pour chaque ligne un compte utilisateur.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone d'ajout d'un compte, un formulaire que vous devez compléter puis valider apparaît.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Les différents statuts sont : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Administrateur : Gérer les comptes et les véhicules
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Enseignant et non enseignant : Visualiser, ajouter, modifier, supprimer ses demandes de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Proviseur Adjoint : Visualiser toutes les demandes de sortie des enseignants et non enseignant du lycée dans un format CSV et dans un tableau où il pourra donner son avis pour chaque demande de sortie, signer électroniquement les autorisations de déplacements et inscrire la date de signature de chaque autorisation de déplacements dans le tableau
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vie scolaire (secrétaire général inclus) : Visualiser toutes les demandes de sortie des enseignants et non enseignant du lycée dans un format CSV et dans un tableau où elle pourra donner son avis pour chaque demande de sortie après que le proviseur adjoint ai donné une réponse favorable
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Economat : Visualiser toutes les demandes de sortie des enseignants et non enseignant du lycée dans un format CSV et dans un tableau où elle pourra donner son avis pour chaque demande de sortie après que le proviseur adjoint et la vie scolaire aient donné une réponse favorable
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Invite (personnel administratif) : Visualiser toutes les demandes de sortie validées dans un format CSV et dans un tableau
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de modification d'un compte,un menu déroulant apparaît où vous devez choisir un compte à modifier puis quand vous cliquez sur "valider" un formulaire pré-rempli apparaît.
				Vous devez faire les modifications nécessaires puis validé
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de suppression d'un compte,un menu déroulant apparaît où vous devez choisir un compte à supprimer puis quand vous cliquez sur "valider" le compte est supprimé et vous êtes redirigez sur le tableau récapitulatif de tous les comptes utilisateurs.
				<br>
				<br>
				<h4 align="center"><b>Gestion de vehicules</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de gestion de vehicules, un menu apparaît et vous devez choisir entre un tableau récapitulatifs de tous les vehicules excepté le bus extérieur, l'ajout d'un vehicule, la modification d'un vehicule et la suppression d'un vehicule en cliquant sur les icones appropriés.
				<br>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatifs, un tableau de tous les vehicules excepté le bus extérieur apparaît avec pour chaque ligne un vehicule.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone d'ajout d'un vehicule, un formulaire que vous devez compléter puis valider apparaît.
                <br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				ATTENTION : NE PAS AJOUTER LE BUS EXTERIEUR CAR IL EXISTE DEJA ! 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de modification d'un vehicule,un menu déroulant apparaît où vous devez choisir un vehicule à modifier puis quand vous cliquez sur "valider" un formulaire pré-rempli apparaît.
				Vous devez faire les modifications nécessaires puis validé
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone de suppression d'un vehicule,un menu déroulant apparaît où vous devez choisir un vehicule à supprimer puis quand vous cliquez sur "valider" le vehicule est supprimé et vous êtes redirigez sur le tableau récapitulatif de tous les vehicules.
				<br>
				<br>
				<h4 align="center"><b>Modification du cout estimatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous souhaitez modifier le cout estimatif, veuillez effectuer les changement dans les fichiers "LEGTA/ajout.php" et "LEGTA/modification.php" de la ligne 269 à la ligne 499.
				<br>
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeenseignant">
				<h2 align="center"><b>Enseignant et non enseignant</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre le tableau récapitulatif, faire une demande, modifier une demande et supprimer une demande en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Tableau Récapitulatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatif, un tableau de toutes les demandes de sortie où vous y êtes impliqué apparaît avec pour chaque ligne une demande de sortie.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le bouton PDF permet de visualiser en détail la demande de sortie sélectionnée au format PDF
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Les colonnes "Proviseur Adjoint", "Vie Scolaire" et "Economat" possedant des boutons permet de connaître leurs avis pour la demande sélectionnée
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button> --> Avis FAVORABLE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button> --> Avis DEFAVORABLE
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de boutons de visibles, c'est que la personne concernée n'a pas encore donné son avis,qu'elle vous a envoyé un mail demandant de modifier votre demande de sortie ou qu'il n'y a pas de reservation de repas si cette personne fait parti de l'economat
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne signature permet de connaître la date à laquelle la demande de sortie sélectionnée à été signée électroniquement par le directeur de l'EPLEFPA ou par le directeur du centre
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si une ligne de la demande de sortie sélectionnée possede un avis défavorable, celle-ci est refusée, cette ligne est de couleur rouge et vous y en êtes informé par mail avec le motif du refus
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie est acceptée lorsque l'arrière plan de celle-ci est de couleur verte et vous avez reçu un mail de confirmation disant que cette demande est accepté
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie peut être acceptée de deux manières : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le proviseur adjoint, la vie scolaire et l'economat ont acceptés votre demande de sortie 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de repas à prévoir, le proviseur adjoint et la vie scolaire ont acceptés votre demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous pouvez également effectuer une recherche, triés les demandes de sortie et modifier le nombre de demandes de sortie affichés dans le tableau
				<br>
				<br>
				<h4 align="center"><b>Faire une demande</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone "Faire une demande", un formulaire que vous devez compléter puis valider apparaît.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Pour sélectionné ou désélectionné plusieurs accompagnateurs tenez la touche CTRL enfoncée tandis que vous cliquez sur les accompagnateurs que vous choisissez.
				Ensuite il ne vous reste qu'à relâcher la touche CTRL
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Toutes les informations concernant le demandeur, le responsable, la date de la demande, le centre qui organise la sortie, le coût estimatif et le nombre de repas pour les accompagnateurs sont remplies automatiquement
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Lorsque vous allez valider le formulaire de demande, un mail va être envoyé au proviseur adjoint lui indiquant de donner son avis concernant cette demande effectuée
				<br>
				<br>
				<h4 align="center"><b>Modifier une demande</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				 - Si vous cliquez sur l'icone de "Modifier une demande",un menu déroulant apparaît où vous devez choisir une demande de sortie que vous avez fait à modifier puis quand vous cliquez sur "valider" un formulaire pré-rempli apparaît.
				 Vous devez faire les modifications nécessaires puis validé 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Pour sélectionné ou désélectionné plusieurs accompagnateurs tenez la touche CTRL enfoncée tandis que vous cliquez sur les accompagnateurs que vous choisissez.
				Ensuite il ne vous reste qu'à relâcher la touche CTRL
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Toutes les informations concernant le responsable, le coût estimatif et le nombre de repas pour les accompagnateurs sont remplies automatiquement
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Lorsque vous allez valider le formulaire de demande, un mail va être envoyé au proviseur adjoint lui indiquant de donner son avis concernant cette demande modifiée car tous les avis ont été supprimées
				<br>
				<br>
				<h4 align="center"><b>Supprimer une demande</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone "Supprimer une demande",un menu déroulant apparaît où vous devez choisir une demande de sortie que vous avez fait à supprimer puis quand vous cliquez sur "valider" la demande de sortie est supprimé , va envoyer un mail à tous ceux qui ont donner un avis leur indiquant que cette demande de sortie est annulée et vous êtes redirigez sur le tableau récapitulatif.
				<br>
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeproviseuradjoint">
				<h2 align="center"><b>Proviseur adjoint</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre le tableau récapitulatifs et le tableau au format CSV en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Tableau Récapitulatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatif, un tableau de toutes les demandes de sortie apparaît avec pour chaque ligne une demande de sortie.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le bouton PDF permet de visualiser en détail la demande de sortie sélectionnée au format PDF et de le signer électroniquement en le téléchargeant
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne "Proviseur Adjoint" permet de visualiser ou de faire votre choix concernant la demande sélectionnée grâce aux boutons cliquables ou pas
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Les colonnes "Vie Scolaire" et "Economat" possedant des boutons permet de connaître leurs avis pour la demande sélectionnée
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button> --> Bouton cliquable avec une action A FAIRE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button> --> Avis FAVORABLE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button> --> Avis DEFAVORABLE
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Lorsque vous avez reçu un mail vous indiquant qu'une demande de sortie vient d'être réalisée ou a été modifiée vous devez donner votre avis à travers le bouton cliquable.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Cette action va vous amener à une page vous indiquant les trois choix possibles à faire puis valider celui que vous avez choisi
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Une fois validé, vous ne pourrez plus changer votre avis sauf si la demande est modifiée
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous accepter la demande, un mail sera envoyé automatiquement à l'economat ou à la vie scolaire s'il n'y a pas de reservation de repas pour, qu'à leur tour, elle donne son avis
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous demander une précision ou vous refusé la demande, un mail sera envoyé automatiquement au demandeur pour connaître le motif de refus ou la précision à apporter et ainsi modifier sa demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne signature vous permet d'indiquer la date à laquelle la demande de sortie sélectionnée à été signée électroniquement grâce à un bouton cliquable lorsque vous avez validé celle-ci
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si une date est déjà écrite, vous ne pouvez pas la changer
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Pour les colonnes "Vie Scolaire" et "Economat", s'il n'y a pas de boutons de visibles, c'est que la personne concernée n'a pas encore donné son avis ou qu'il n'y a pas de reservation de repas si la personne concernée est l'economat
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si une ligne de la demande de sortie sélectionnée possede un avis défavorable, celle-ci est refusée, cette ligne est de couleur rouge et vous y en êtes informé par mail avec le motif du refus
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie est acceptée lorsque l'arrière plan de celle-ci est de couleur verte et vous avez reçu un mail de confirmation disant que cette demande est accepté
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie peut être acceptée de deux manières : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous, la vie scolaire et l'economat avez acceptés une demande de sortie 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de repas à prévoir, vous et la vie scolaire avez acceptés une demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous pouvez également effectuer une recherche, triés les demandes de sortie et modifier le nombre de demandes de sortie affichés dans le tableau
				<br>
				<br>
				<h4 align="center"><b>Tableau au format CSV</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau au format CSV, vous devez indiquer une période du tableau à télécharger au format CSV, valider, puis cliquer sur l'icone du tableau au format CSV pour le télécharger.
				<br>
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeviescolaire">
				<h2 align="center"><b>Vie Scolaire</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre le tableau récapitulatifs et le tableau au format CSV en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Tableau Récapitulatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatif, un tableau de toutes les demandes de sortie validé par le proviseur adjoint apparaît avec pour chaque ligne une demande de sortie.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le bouton PDF permet de visualiser en détail la demande de sortie sélectionnée au format PDF
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne "Vie Scolaire" permet de visualiser ou de faire votre choix concernant la demande sélectionnée grâce aux boutons cliquables ou pas
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Les colonnes "Proviseur adjoint" et "Economat" possedant des boutons permet de connaître leurs avis pour la demande sélectionnée
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button> --> Bouton cliquable avec une action A FAIRE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button> --> Avis FAVORABLE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button> --> Avis DEFAVORABLE
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Lorsque vous avez reçu un mail vous indiquant qu'une demande de sortie vient d'être réalisée, vous devez donner votre avis à travers le bouton cliquable.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Cette action va vous amener à une page vous indiquant les trois choix possibles à faire puis valider celui que vous avez choisi
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Une fois validé, vous ne pourrez plus changer votre avis sauf si la demande est modifiée
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous accepter la demande et que celle-ci prévoit des repas, un mail sera envoyé automatiquement au demandeur, au proviseur adjoint et à l'economat afin de leur confirmer que la demande de sortie à été validée par tous
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous accepter la demande et que celle-ci ne prévoit pas de repas, un mail sera envoyé automatiquement au demandeur et au proviseur adjoint afin de leur confirmer que la demande de sortie à été validée par tous
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous demander une précision, un mail sera envoyé automatiquement au demandeur pour connaître la précision à apporter et ainsi modifier sa demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous refusé la demande, un mail sera envoyé automatiquement au demandeur, au proviseur adjoint et à l'economat s'il y a une reservation de repas pour connaître le motif de refus
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne signature permet de connaître la date à laquelle la demande de sortie sélectionnée à été signée électroniquement par le directeur de l'EPLEFPA ou par le directeur du centre
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Pour la colonne "Economat", s'il n'y a pas de boutons de visibles, c'est que la personne concernée n'a pas encore donné son avis ou qu'il n'y a pas de repas pour cette demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si une ligne de la demande de sortie sélectionnée possede un avis défavorable, celle-ci est refusée, cette ligne est de couleur rouge et vous y en êtes peut-être informé par mail avec le motif du refus
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie est acceptée lorsque l'arrière plan de celle-ci est de couleur verte et vous avez peut-être reçu un mail de confirmation disant que cette demande est accepté
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie peut être acceptée de deux manières : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous, le proviseur adjoint et l'economat avez acceptés une demande de sortie 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de repas à prévoir, vous et le proviseur adjoint avez acceptés une demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous pouvez également effectuer une recherche, triés les demandes de sortie et modifier le nombre de demandes de sortie affichés dans le tableau
				<br>
				<br>
				<h4 align="center"><b>Tableau au format CSV</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau au format CSV, vous devez indiquer une période du tableau à télécharger au format CSV, valider, puis cliquer sur l'icone du tableau au format CSV pour le télécharger.
				<br>
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeeconomat">
				<h2 align="center"><b>Economat</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre le tableau récapitulatifs et le tableau au format CSV en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Tableau Récapitulatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatif, un tableau de toutes les demandes de sortie validé par le proviseur adjoint et la vie scolaire apparaît avec pour chaque ligne une demande de sortie.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le bouton PDF permet de visualiser en détail la demande de sortie sélectionnée au format PDF
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne "Economat" permet de visualiser ou de faire votre choix si la demande sélectionnée à besoin de repas grâce aux boutons cliquables ou pas
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Les colonnes "Proviseur adjoint" et "Vie Scolaire" possedant des boutons permet de connaître leurs avis positifs pour la demande sélectionnée
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button> --> Bouton cliquable avec une action A FAIRE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button> --> Avis FAVORABLE
				</div>
				<div>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button> --> Avis DEFAVORABLE
				</div>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Lorsque vous avez reçu un mail vous indiquant qu'une demande de sortie vient d'être réalisée, vous devez donner votre avis à travers le bouton cliquable.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Cette action va vous amener à une page vous indiquant les trois choix possibles à faire puis valider celui que vous avez choisi
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				Une fois validé, vous ne pourrez plus changer votre avis sauf si la demande est modifiée
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous accepter la demande, un mail sera envoyé automatiquement à la vie scolaire pour, qu'à leur tour, elle donne son avis
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous demander une précision, un mail sera envoyé automatiquement au demandeur pour connaître la précision à apporter et ainsi modifier sa demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous refusé la demande, un mail sera envoyé automatiquement au demandeur, au proviseur adjoint et à la vie scolaire pour connaître le motif de refus
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne signature permet de connaître la date à laquelle la demande de sortie sélectionnée à été signée électroniquement par le directeur de l'EPLEFPA ou par le directeur du centre
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si une ligne de la demande de sortie sélectionnée possede un avis défavorable, celle-ci est refusée, cette ligne est de couleur rouge
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie est acceptée lorsque l'arrière plan de celle-ci est de couleur verte
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie peut être acceptée de deux manières : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous, le proviseur adjoint et la vie scolaire avez acceptés une demande de sortie 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de repas à prévoir, la vie scolaire et le proviseur adjoint ont acceptés une demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous pouvez également effectuer une recherche, triés les demandes de sortie et modifier le nombre de demandes de sortie affichés dans le tableau
				<br>
				<br>
				<h4 align="center"><b>Tableau au format CSV</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau au format CSV, vous devez indiquer une période du tableau à télécharger au format CSV, valider, puis cliquer sur l'icone du tableau au format CSV pour le télécharger.
				<br>
				<br>
				</p>
			</div>
			<hr>
			<div id="lyceeinvite">
				<h2 align="center"><b>Invite</b></h2>
				<br>
				<p>
				Lorsque vous êtes sur la page d'accueil, vous devez choisir entre le tableau récapitulatifs et le tableau au format CSV en cliquant sur les icones appropriés.
				<br>
				<h4 align="center"><b>Tableau Récapitulatif</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau récapitulatif, un tableau de toutes les demandes de sortie validée apparaît avec pour chaque ligne une demande de sortie.
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Le bouton PDF permet de visualiser en détail la demande de sortie sélectionnée au format PDF
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne "Economat" possedant des boutons permet de connaître leurs avis positifs pour la demande sélectionnée ou s'il n'y a pas de bouton, c'est qu'il n'y a pas de repas pour cette demande
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Les colonnes "Proviseur adjoint" et "Vie Scolaire" possedant des boutons permet de connaître leurs avis positifs pour la demande sélectionnée
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- La colonne signature permet de connaître la date à laquelle la demande de sortie sélectionnée à été signée électroniquement par le directeur de l'EPLEFPA ou par le directeur du centre
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie est acceptée lorsque l'arrière plan de celle-ci est de couleur verte
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Une demande de sortie peut être acceptée de deux manières : 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous, le proviseur adjoint et la vie scolaire avez acceptés une demande de sortie 
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- S'il n'y a pas de repas à prévoir, la vie scolaire et le proviseur adjoint ont acceptés une demande de sortie
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Vous pouvez également effectuer une recherche, triés les demandes de sortie et modifier le nombre de demandes de sortie affichés dans le tableau
				<br>
				<br>
				<h4 align="center"><b>Tableau au format CSV</b></h4>
				<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				- Si vous cliquez sur l'icone du tableau au format CSV, vous devez indiquer une période du tableau à télécharger au format CSV, valider, puis cliquer sur l'icone du tableau au format CSV pour le télécharger.
				<br>
				<br>
				</p>
			</div>
		</div>
	</div>
</div>